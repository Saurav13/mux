@extends('layouts.main')
@section('title','| Services')
@section('body')
<section class="container u-ns-bg-v1-bottom g-bg-white g-pt-10 g-pb-50">
    <div class="row justify-content-center g-mb-20">
      <div class="col-lg-7">
        <!-- Heading -->
        <div class="text-center mb-4">
          <h2 class="h3 text-uppercase mb-3">Our services</h2>
          <div class="d-inline-block g-width-60 g-height-1 g-bg-black mb-2"></div>
          <p class="lead mb-0">We are a creative studio focusing on culture, luxury, editorial &amp; art. Somewhere between sophistication and simplicity.</p>
        </div>
        <!-- End Heading -->
      </div>
    </div>

    <!-- Icon Blocks -->
    <div class="row">
      <div class="col-md-4 g-mb-30">
        <!-- Icon Blocks -->
        <div class="g-brd-around--md g-brd-gray-light-v4 text-center g-pa-10 g-px-30--lg g-py-40--lg">
          <span class="d-block g-color-gray-dark-v4 g-font-size-40 g-mb-15">
            <i class="icon-education-087 u-line-icon-pro"></i>
          </span>
        <a href="{{URL::to('category')}}" style="text-decoration:none;">
          <h3 class="h5 g-color-black g-mb-10">Sublimation Printing</h3>
        </a>
          <p class="g-color-gray-dark-v4">We strive to embrace and drive change in our industry which allows us to keep our clients relevant and ready to adapt.</p>
          <div class="d-inline-block g-width-40 g-brd-bottom g-brd-gray-dark-v5 g-my-15"></div>
          <ul class="list-unstyled g-px-30 g-mb-0">
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Mug</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Light Color T-shirts</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Bottle</a></li>
          <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Jersey</a></li>
          <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Beer Glasses</a></li>
          <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Mouse Pad</a></li>
          </ul>
        </div>
        <!-- End Icon Blocks -->
      </div>
      <div class="col-md-4 g-mb-30">
        <!-- Icon Blocks -->
        <div class="g-brd-around--md g-brd-gray-light-v4 text-center g-pa-10 g-px-30--lg g-py-40--lg">
          <span class="d-block g-color-gray-dark-v4 g-font-size-40 g-mb-15">
            <i class="icon-finance-009 u-line-icon-pro"></i>
          </span>
          <a href="{{URL::to('category')}}" style="text-decoration:none;">
            <h3 class="h5 g-color-black g-mb-10">UV Printing</h3>
          </a>
          <p class="g-color-gray-dark-v4">We strive to embrace and drive change in our industry which allows us to keep our clients relevant and ready to adapt.</p>
          <div class="d-inline-block g-width-40 g-brd-bottom g-brd-gray-dark-v5 g-my-15"></div>
          <ul class="list-unstyled g-px-30 g-mb-0">
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Pen</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">PVC Cords</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Mobile Covers</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Small Bell</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Acrylic Board</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Glass</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Laptop</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Tiles</a></li>
          </ul>
        </div>
        <!-- End Icon Blocks -->
      </div>
      <div class="col-md-4 g-mb-30">
        <!-- Icon Blocks -->
        <div class="g-brd-around--md g-brd-gray-light-v4 text-center g-pa-10 g-px-30--lg g-py-40--lg">
          <span class="d-block g-color-gray-dark-v4 g-font-size-40 g-mb-15">
            <i class="icon-finance-256 u-line-icon-pro"></i>
          </span>
          <h3 class="h5 g-color-black g-mb-10">Eco Solvent Printing</h3>
          <p class="g-color-gray-dark-v4">We strive to embrace and drive change in our industry which allows us to keep our clients relevant and ready to adapt.</p>
          <div class="d-inline-block g-width-40 g-brd-bottom g-brd-gray-dark-v5 g-my-15"></div>
          <ul class="list-unstyled g-px-30 g-mb-0">
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">">Flex and Banners</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Transparent Sticker</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">One way vision</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Light Board</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Matt Flex</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Canvas</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Wallpaper</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Curtains</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Satin</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Reflective Flex</a></li>
            <li class="g-brd-bottom g-brd-gray-light-v3 g-py-10">Reflextive Sticker</a></li>
          </ul>
        </div>
        <!-- End Icon Blocks -->
      </div>
    </div>
    <!-- End Icon Blocks -->
  </section>
@endsection