@extends('layouts.main')

@section('css')
<link  rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.css">
<link  rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
<link  rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
@endsection

@section('body')
{{-- <section class="g-bg-cover g-flex-centered g-bg-pos-top-center g-bg-img-hero g-bg-black-opacity-0_3--after" style="background-image: url(/img/main2.jpg); height: calc(100vh - 75px);" data-calc-target="#js-header">
   
</section> --}}
<section class="g-pos-rel">
    <!-- Promo Block - Slider -->
    <div class="js-carousel" data-autoplay="true" data-infinite="true" data-fade="true" data-speed="5000">
      <div class="js-slide g-bg-cover g-bg-pos-top-center g-bg-img-hero g-bg-black-opacity-0_3--after" style="background-image: url(/img/main2.jpg);" data-calc-target="#js-header"></div>
      <div class="js-slide g-bg-cover g-bg-pos-top-center g-bg-img-hero g-bg-black-opacity-0_3--after" style="background-image: url(/img/main5.jpeg);" data-calc-target="#js-header"></div>
    </div>
    <!-- End Promo Block - Slider -->

    <!-- Promo Block Content -->
    <div class="g-absolute-centered--y g-left-0 g-right-0">
      <div class="container g-z-index-1">
        <div class="row">
              <div class="col-lg-7 align-self-center g-color-white">
                  <div class="g-px-50--lg">
                      <h2 class="g-color-white g-font-weight-600 g-font-size-40 g-font-size-50--md text-uppercase g-line-height-1_2 g-mb-20">Mux
                      <br> Mobile Reparing</h2>
                      <p class="lead g-color-white-opacity-0_8 g-mb-35">This is where we sit down, grab a cup of coffee and dial in the details. Understanding the task at hand and ironing out the wrinkles is key.</p>
                      <a class="btn btn-lg u-btn-primary g-font-weight-600 g-font-size-13 text-uppercase g-rounded-5 g-px-25 g-py-15" href="#">Hire Unify</a>
                  </div>
              </div>
          <div class="col-lg-5 align-self-center g-mb-40 g-mb-0--lg">
            <div class="g-bg-white g-rounded-5 g-px-40 g-py-20">
              <h3 class="h6 g-color-black g-font-weight-600 text-uppercase text-center g-mb-10 g-font-size-20">Book A Repair</h3>
  
              <!-- Promo Block - Form -->
              <form>
                <div class="form-group g-mb-20">
                  <input class="form-control g-brd-gray-light-v5 g-bg-gray-light-v5 g-bg-gray-light-v5--focus g-px-10 g-py-12" type="text" placeholder="Full Name">
                </div>
                <div class="form-group g-mb-20">
                  <input class="form-control g-brd-gray-light-v5 g-bg-gray-light-v5 g-bg-gray-light-v5--focus g-px-10 g-py-12" type="text" placeholder="Email">
                </div>
                  <div class="form-group g-mb-20">
                  <input class="form-control g-brd-gray-light-v5 g-bg-gray-light-v5 g-bg-gray-light-v5--focus g-px-10 g-py-12" type="text" placeholder="Email">
                </div>
              
  
              
  
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group g-mb-20">
                      <input class="form-control g-brd-gray-light-v5 g-bg-gray-light-v5 g-bg-gray-light-v5--focus g-px-10 g-py-12" type="text" placeholder="Name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group g-mb-20">
                      <input class="form-control g-brd-gray-light-v5 g-bg-gray-light-v5 g-bg-gray-light-v5--focus g-px-10 g-py-12" type="email" placeholder="Email">
                    </div>
                  </div>
                </div>
  
                <button class="btn btn-lg btn-block u-btn-primary g-font-weight-600 g-font-size-13 text-uppercase g-rounded-5 g-py-13" type="submit">Book Now</button>
              </form>
              <!-- End Promo Block - Form -->
            </div>
          </div>
  
          <!-- Promo Block - Info -->
          
          <!-- End Promo Block - Info -->
        </div>
      </div>
    </div>
    <!-- End Promo Block Content -->
  </section>
<section class="g-my-40 g-mx-60 ">
    <div class="text-center mx-auto g-max-width-600 g-mb-50">
        <h2 class="g-color-black mb-4">How It Works</h2>
        <p class="lead">We want to create a range of beautiful, practical and modern outerwear that doesn't cost the earth – but let's you still live life in style.</p>
    </div>
    <ul class="row list-inline u-info-v9-1 mb-0">
        <li class="col-md-3 list-inline-item g-mx-0 g-mb-30">
        <!-- Icon Blocks -->
            <div class="u-block-hover text-center g-px-40">
                <div class="g-mb-5">
                    <span class="u-icon-v1 g-width-85 g-height-85 g-color-main g-font-size-50 rounded-circle">
                        <i class="icon-education-024 u-line-icon-pro"></i>
                    </span>
                </div>
                <div class="g-mb-25">
                    <span class="u-icon-v2 u-shadow-v22 g-width-25 g-height-25 g-color-primary g-bg-white g-font-size-9 rounded-circle">
                        <i class="fa fa-check"></i>
                    </span>
                </div>
                <h3 class="g-color-primary g-font-weight-600 g-font-size-17 text-uppercase mb-3">
                    <span class="g-color-main g-font-weight-700">01.</span>
                    Book Repair Service
                </h3>
                <p>We aim high at being focused on building relationships with our clients and community.</p>
            </div>
        <!-- End Icon Blocks -->
        </li>
    
        <li class="col-md-3 list-inline-item g-mx-0 g-mb-30">
        <!-- Icon Blocks -->
            <div class="u-block-hover text-center g-px-40">
                <div class="g-mb-5">
                    <span class="u-icon-v1 g-width-85 g-height-85 g-color-main g-font-size-50 rounded-circle">
                        <i class="icon-education-073 u-line-icon-pro"></i>
                    </span>
                </div>
                <div class="g-mb-25">
                    <span class="u-icon-v2 u-shadow-v22 g-width-25 g-height-25 g-color-white g-bg-primary g-font-size-9 rounded-circle">
                        <i class="fa fa-check"></i>
                    </span>
                </div>
                <h3 class="g-color-primary g-font-weight-600 g-font-size-17 text-uppercase mb-3">
                    <span class="g-color-main g-font-weight-700">02.</span>
                    Free PickUp
                </h3>
                <p>We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p>
            </div>
        <!-- End Icon Blocks -->
        </li>
    
        <li class="col-md-3 list-inline-item g-mx-0 g-mb-30">
        <!-- Icon Blocks -->
            <div class="u-block-hover text-center g-px-40">
                <div class="g-mb-5">
                    <span class="u-icon-v1 g-width-85 g-height-85 g-color-main g-font-size-50 rounded-circle">
                        <i class="icon-communication-180 u-line-icon-pro"></i>
                    </span>
                </div>
                <div class="g-mb-25">
                    <span class="u-icon-v2 u-shadow-v22 g-width-25 g-height-25 g-color-primary g-bg-white g-font-size-9 rounded-circle">
                        <i class="fa fa-check"></i>
                    </span>
                </div>
                <h3 class="g-color-primary g-font-weight-600 g-font-size-17 text-uppercase mb-3">
                    <span class="g-color-main g-font-weight-700">03.</span>
                    Repair At our Lab
                </h3>
                <p>At the end of the day, it's important to not let being busy distract us from having fun.</p>
            </div>
        <!-- End Icon Blocks -->
        </li>

        <li class="col-md-3 list-inline-item g-mx-0 g-mb-30">
        <!-- Icon Blocks -->
            <div class="u-block-hover text-center g-px-40">
                <div class="g-mb-5">
                    <span class="u-icon-v1 g-width-85 g-height-85 g-color-main g-font-size-50 rounded-circle">
                        <i class="icon-communication-180 u-line-icon-pro"></i>
                    </span>
                </div>
                <div class="g-mb-25">
                    <span class="u-icon-v2 u-shadow-v22 g-width-25 g-height-25 g-color-primary g-bg-white g-font-size-9 rounded-circle">
                        <i class="fa fa-check"></i>
                    </span>
                </div>
                <h3 class="g-color-primary g-font-weight-600 g-font-size-17 text-uppercase mb-3">
                    <span class="g-color-main g-font-weight-700">03.</span>
                    Free and Fast Return
                </h3>
                <p>At the end of the day, it's important to not let being busy distract us from having fun.</p>
            </div>
        <!-- End Icon Blocks -->
        </li>
    </ul>
</section>

<section>
        <div class="dzsparallaxer auto-init height-is-based-on-content use-loading" data-options="{
            settings_mode_oneelement_max_offset: '150'
           }">
        <!-- Parallax Image -->
        <div class="divimage dzsparallaxer--target w-100" style="height: 140%; background-image: url(/img/main.jpg)"></div>
      
        <section class="g-flex-centered g-height-500 g-color-white u-bg-overlay g-bg-black-opacity-0_6--after g-py-20">
          <div class="container u-bg-overlay__inner">
            <div class="row">
              <div class="col-md-6 align-self-center g-py-20">
                <h2 class="h4 text-uppercase g-letter-spacing-1 g-mb-20">Why Choose MUX?</h2>
                <p class="lead g-color-white mb-0 g-line-height-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin hendrerit rhoncus tempus. Donec id orci malesuada, finibus odio quis, tincidunt libero. Fusce in venenatis ligula. Etiam eget lacus id erat scelerisque tempor.</p>
              </div>
      
              <div class="col-md-6 align-self-center g-py-20">
                <img class="w-100" src="/img/main3.jpg" alt="Iamge Description">
              </div>
            </div>
          </div>
        </section>
      </div>
</section>

<section class="g-my-40 g-mx-60 ">
   <div class="text-center mx-auto g-max-width-600 g-mb-50">
          <h2 class="g-color-black mb-4">Our Customers</h2>
          <p class="lead">We want to create a range of beautiful, practical and modern outerwear that doesn't cost the earth – but let's you still live life in style.</p>
    </div>
    <div class="row no-gutters">
  <div class="col-md-6 col-lg-4 g-brd-right g-brd-gray-light-v4 g-mb-30">
    <!-- Testimonials -->
    <div class="u-shadow-v20--hover g-bg-white--hover text-center g-rounded-4 g-transition-0_3 g-pa-30">
      <blockquote class="lead u-blockquote-v6 g-font-style-italic g-mb-35">Your customer support is the best I have experienced with any theme I have purchased. You have a theme that far exceeds all others. Thanks for providing such a fantastic theme, all your efforts are greatly appreciated on our end.</blockquote>
      <img class="img-thumbnail g-width-70 g-height-70 g-brd-gray-light-v7 rounded-circle g-mb-20" src="/frontend-assets/main-assets/assets/img-temp/100x100/img14.jpg" alt="Image Description">
      <h4 class="h5 g-color-black g-mb-0">Lewis Dean</h4>
      <em class="g-color-gray-dark-v4 g-font-style-normal">Google Inc.</em>
    </div>
    <!-- End Testimonials -->
  </div>

  <div class="col-md-6 col-lg-4 g-brd-right g-brd-gray-light-v4 g-mb-30">
    <!-- Testimonials -->
    <div class="u-shadow-v20--hover g-bg-white--hover text-center g-rounded-4 g-transition-0_3 g-pa-30">
      <blockquote class="lead u-blockquote-v6 g-font-style-italic g-mb-35">Thanks for this great update!!! However it's somewhat always a bit frustrating when I spent a lot of time trying to add features that were missing by myself and seeing updates including it in a way which is 300% nicer. Great job guys!</blockquote>
      <img class="img-thumbnail g-width-70 g-height-70 g-brd-gray-light-v7 rounded-circle g-mb-20" src="/frontend-assets/main-assets/assets/img-temp/100x100/img5.jpg" alt="Image Description ">
      <h4 class="h5 g-color-black g-mb-0 ">Julia Cooper</h4>
      <em class="g-color-gray-dark-v4 g-font-style-normal">Yahoo Co.</em>
    </div>
    <!-- End Testimonials -->
  </div>

  <div class="col-md-6 col-lg-4 g-mb-30">
    <!-- Testimonials -->
    <div class="u-shadow-v20--hover g-bg-white--hover text-center g-rounded-4 g-transition-0_3 g-pa-30">
      <blockquote class="lead u-blockquote-v6 g-font-style-italic g-mb-35 ">Your customer support is the best I have experienced with any theme I have purchased. You have a theme that far exceeds all others. Thanks for providing such a fantastic theme, all your efforts are greatly appreciated on our end.</blockquote>
      <img class="img-thumbnail g-width-70 g-height-70 g-brd-gray-light-v7 rounded-circle g-mb-20" src="/frontend-assets/main-assets/assets/img-temp/100x100/img7.jpg" alt="Image Description">
      <h4 class="h5 g-color-black g-mb-0">Jack Powell</h4>
      <em class="g-color-gray-dark-v4 g-font-style-normal">Apple Ltd.</em>
    </div>
    <!-- End Testimonials -->
  </div>
</div>
</section>

@endsection

@section('js')

{{-- <script src="/frontend-assets/main-assets/assets/vendor/jquery-ui/ui/widgets/slider.js"></script> --}}

<script src="{{asset('frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{asset('frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>

<script src="{{asset('frontend-assets/main-assets/assets/vendor/revolution-slider/revolution-addons/typewriter/js/revolution.addon.typewriter.min.js') }}"></script>

<script src="{{asset('frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script src="{{asset('frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
<script src="{{asset('frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script src="{{asset('frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{asset('frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script src="{{asset('frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script src="{{asset('frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script src="{{asset('frontend-assets/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>

<script  src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script  src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
<script  src="/frontend-assets/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>
<script >
    $(document).on('ready', function () {
        // initialization of parallax
      $.HSCore.components.HSCarousel.init('.js-carousel');

        $('.dzsparallaxer').dzsparallaxer();
    });
</script>
@endsection




