@extends('layouts.main')

@section('body')

@section('title','| Cart')


    <!-- Breadcrumbs -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
            <ul class="u-list-inline">
                <li class="list-inline-item g-mr-5">
                <a class="u-link-v5 g-color-text" href="{{URL::to('home')}}">Home</a>
                <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                </li>
                <li class="list-inline-item g-color-primary">
                <span>Cart</span>
                </li>
            </ul>
        </div>
    </section>
    <!-- End Breadcrumbs -->

    <div class="container g-pt-100 g-pb-70">
        <div class="row">
            <div class="col-md-12 g-mb-30">
                <!-- Products Block -->
                <div class="g-overflow-x-scroll g-overflow-x-visible--lg">
                    <table class="text-center w-100">
                        <thead class="h6 g-brd-bottom g-brd-gray-light-v3 g-color-black text-uppercase">
                            <tr>
                                <th class="g-font-weight-400 text-left g-pb-20">Product</th>
                                <th class="g-font-weight-400 g-width-130 g-pb-20">Price</th>
                                <th class="g-font-weight-400 g-width-150 g-pb-20">Qty</th>
                                <th class="g-font-weight-400 g-width-130 g-pb-20">Total</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($cart_items as $item)
                                <!-- Item-->
                                <tr class="g-brd-bottom g-brd-gray-light-v3" id="item-{{ $item->rowId }}">
                                    <td class="text-left g-py-25">
                                        <img class="d-inline-block g-width-100 mr-4" src="{{ $item->options['image'] }}" alt="{{ $item->name }}">
                                        <div class="d-inline-block align-middle">
                                            <h4 class="h6 g-color-black">{{ $item->name }}</h4>
                                            <ul class="list-unstyled g-color-gray-dark-v4 g-font-size-12 g-line-height-1_6 mb-0">
                                                {{-- <li>Color: Black</li>
                                                <li>Size: MD</li> --}}
                                            </ul>
                                        </div>
                                    </td>
                                    <td class="g-color-gray-dark-v2 g-font-size-13">Rs. {{ $item->price }}</td>
                                    <td>
                                        <div class="js-quantity input-group u-quantity-v1 g-brd-primary--focus">
                                            <button class="btn downnum" title="Remove One">
                                                <i class="fa fa-fw fa-minus"></i>
                                            </button>
                                            <input class="js-result form-control text-center g-font-size-13 rounded-0 g-pa-0 cart-quantity" type="text" value="{{ $item->qty }}" data-id="{{ $item->rowId }}" style="width:40px" readonly>
                                            <button class="btn upnum" title="Add One">
                                                <i class="fa fa-fw fa-plus"></i>
                                            </button>
                                            {{-- <div class="input-group-addon d-flex align-items-center g-width-30 g-brd-gray-light-v2 g-bg-white g-font-size-12 rounded-0 g-px-5 g-py-6">
                                                <a><i class="js-plus g-color-gray g-color-primary--hover fa fa-angle-up upnum"></i></a>
                                                <i class="js-minus g-color-gray g-color-primary--hover fa fa-angle-down downnum"></i></a>
                                            </div> --}}
                                        </div>
                                    </td>
                                    <td class="text-right g-color-black">
                                        <span class="g-color-gray-dark-v2 g-font-size-13 mr-4">Rs. <span id="item-total-{{ $item->rowId }}">{{ $item->price * $item->qty }}</span></span>
                                        <button style="background-color: transparent; border: none;" class="g-color-gray-dark-v4 g-color-black--hover g-cursor-pointer delete-item" data-href="{{ url('/cart/removeitem/'.$item->rowId) }}">
                                            <i class="mt-auto fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                <!-- End Item-->
                            @endforeach
                            <tr class="border-bottom">
                                <td class="table-image"></td>
                                <td style="padding: 40px;"></td>
                                <td class="small-caps table-bg"
                                    style="text-align: right">Sub Total</td>
                                <td id="total">{{ Cart::subtotal() }}</td>
                                <td class="column-spacer"></td>
                                <td></td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <!-- End Products Block -->
            </div>
            <div style="col-md-6">
                <a href="{{ url('shop') }}" class="btn btn-primary btn">Continue Shopping</a> &nbsp;

            </div>
            <div style="col-md-6" style="text-align:right">

                <a href="{{ url('checkout') }}" class="btn btn-success btn">Checkout</a>

                <form method="POST" action="{{ url('cart/emptycart') }}" style="display:inline-block">
                    {{ csrf_field() }} 
                    <button type="submit" class="btn btn-danger">
                        <i class="fa fa-fw fa-trash"></i>Emprt Cart
                    </button>
                </form>
            </div>
        </div>
    </div>
  
@endsection

@section('js')

<script>

    $(document).ready(function(){
        $('.upnum').click(function(){
            var ele = $(this);
            var quantity = ele.siblings('.cart-quantity');
            
            var v = parseInt(quantity.val()) + 1;
            var rowId = quantity.data('id');

            ele.attr('disabled','disabled');
            ele.siblings('button').attr('disabled','disabled');
            
            $.ajax({
                type: "POST",
                url: "{{ URL::to('/cart/updateqty') }}",
                data: { rowId: rowId, _token:'{{ csrf_token() }}', quantity: v },
                success: function(response)
                {
                    ele.removeAttr('disabled');
                    ele.siblings('button').removeAttr('disabled');

                    updateCart(response);
                },
                error:function(response)
                {
                    ele.removeAttr('disabled');
                    ele.siblings('button').removeAttr('disabled');

                    showErrors(response);
                }
            });

        });

        $('.downnum').click(function(){
            var ele = $(this);
            var quantity = ele.siblings('.cart-quantity');
            var rowId = quantity.data('id');

            var v = parseInt(quantity.val()) - 1;
            if(v <= 0) return;
            
            ele.attr('disabled','disabled');
            ele.siblings('button').attr('disabled','disabled');
            
            $.ajax({
                type: "POST",
                url: "{{ URL::to('/cart/updateqty') }}",
                data: { rowId: rowId, _token:'{{ csrf_token() }}', quantity: v },
                success: function(response)
                {
                    ele.removeAttr('disabled');
                    ele.siblings('button').removeAttr('disabled');

                    updateCart(response);
                },
                error:function(response)
                {
                    ele.removeAttr('disabled');
                    ele.siblings('button').removeAttr('disabled');

                    showErrors(response);
                }
            });
        });

        function updateCart(response) {
            $('#item-' + response.item.rowId + ' .cart-quantity').val(response.item.qty);
            $('#item-total-' + response.item.rowId).html(response.item.price*response.item.qty);
            $('#total').html(response.total);
        }

        $('.delete-item').click(function(){
            
            var ele = $(this);

            ele.attr('disabled','disabled');
            
            $.ajax({
                type: "POST",
                url: ele.data('href'),
                data: { _token:'{{ csrf_token() }}' },
                success: function(response)
                {
                    ele.removeAttr('disabled');

                    $('#item-' + response.rowId).fadeOut("normal", function () {
                        $(this).remove();
                    });
                    $('#total').html(response.total);

                    $('#cartTotal').text(response.count);

                    if (response.total == 0)
                        location.reload();
                },
                error:function(response)
                {
                    ele.removeAttr('disabled');

                    showErrors(response);
                }
            });
        })
    })
</script>
@endsection