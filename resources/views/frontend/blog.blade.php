@extends('layouts.main') 
@section('body')

<div class="container g-pt-100 g-pb-70">
    <div class="row justify-content-center">
        <div class="col-lg-10">

          @foreach ($posts as $post)
              <!-- Blog Minimal Blocks -->
            <article class="g-mx-15 u-block-hover">
              <div class="g-px-100--md g-py-30--md">
                <ul class="list-inline g-color-gray-dark-v4 g-font-weight-600 g-font-size-12">
                  <li class="list-inline-item mr-0">{{ $post->author }}</li>
                  <li class="list-inline-item mx-2">·</li>
                  <li class="list-inline-item">{{ date('M j, Y',strtotime($post->updated_at)) }}</li>
                </ul>
                <h2 class="h2 g-color-black g-font-weight-600 mb-4">
                    <a class="u-link-v5 g-color-black g-color-primary--hover" href="{{ route('blog',$post->slug) }}">{{ $post->title }}</a>
                  </h2>
              </div>
              <div class="g-overflow-hidden">
                <img class="img-fluid w-100 u-block-hover__main--mover-down" src="{{ route('optimize', ['blog_images',$post->image,900,400]) }}" alt="{{ $post->title }}">
              </div>
              <div class="g-px-100--md g-py-30--md">
                <div class="mb-4">
                  <p class="g-font-size-18 g-line-height-2 mb-0">
                    {{ substr(strip_tags($post->content),0,250) }} ...
                  </p>
                </div>
                <a class="g-color-gray-dark-v2 g-color-primary--hover g-font-weight-600 g-font-size-12 g-text-underline--none--hover text-uppercase" href="{{ route('blog',$post->slug) }}">Read more</a>
              </div>
            </article>
            <!-- End Blog Minimal Blocks -->
  
            <hr class="g-mb-60 g-mx-15">
          @endforeach
          
          <!-- Pagination -->
          <nav class="g-mb-100" aria-label="Page Navigation" style="text-align:center">
              {{ $posts->links('frontend.partials.paginate') }}
          </nav>
          <!-- End Pagination -->
        </div>

    </div>
</div>
@endsection
