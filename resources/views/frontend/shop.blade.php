@extends('layouts.main') 
@section('body')
@section('title','| Shop')
    <style>
        .inner-list{
            display: none;
        }
        .inner-list.show{
            display: block;
        }
        button:focus {
            outline: none;
        }
    </style>
    
    <!-- Products -->
    <div class="container">
        <div class="row g-pb-20">
            <!-- Content -->
            <div class="col-md-9 order-md-2">
                <div class="g-pl-15--lg" >

                    <!-- Products -->
                    <div class="row g-pt-30 g-mb-50">
                        @foreach ($products as $item)
                            <div class="col-6 col-lg-4 g-mb-30">
                                <!-- Product -->
                                <figure class="g-pos-rel g-mb-20">
                                    <a href="{{URL::to('products'.'/'.$item->slug)}}">
                                        <img style="max-height: 20rem; width: 20rem;" class="img-fluid" src="{{ $item->firstImage }}" alt="Image Description">
                                    </a>  
                                    @if($item->featured)
                                        <figcaption class="w-100 g-bg-primary g-bg-black--hover text-center g-pos-abs g-bottom-0 g-transition-0_2 g-py-5">
                                            <a class="g-color-white g-font-size-11 text-uppercase g-letter-spacing-1 g-text-underline--none--hover" href="#!">Featured</a>
                                        </figcaption>
                                    @endif
                                </figure>

                                <div class="media">
                                    <!-- Product Info -->
                                    <div class="d-flex flex-column">
                                        <h4 class="h6 g-color-black mb-1">
                                            <a class="u-link-v5 g-color-black g-color-primary--hover" href="{{URL::to('products'.'/'.$item->slug)}}">
                                                {{$item->product_name}}
                                            </a>
                                        </h4>
                                        <a href="/shop?filter={{$item->subcategory->slug}}" class="d-inline-block g-color-gray-dark-v5 g-font-size-13">{{$item->subcategory->subcat_name}}</a>
                                        <span class="d-block g-color-black g-font-size-17">Rs.{{ number_format($item->price) }} per roll</span>
                                    </div>
                                    <!-- End Product Info -->
        
                                    <!-- Products Icons -->
                                    <ul class="list-inline media-body text-right">
                                        
                                        <li class="list-inline-item align-middle mx-0">
                                            <button data-id="{{ $item->id }}" class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle addToCart"
                                                style="background-color: transparent;border: none;cursor:pointer"
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                title="Place Order">
                                                <i class="icon-finance-100 u-line-icon-pro" style="margin-top:3px"></i>
                                            </button>
                                        </li>
                                    </ul>
                                    <!-- End Products Icons -->
                                </div>
                                <!-- End Product -->
                            </div>
                        @endforeach
                    </div>
                    <!-- End Products -->

                    <hr class="g-mb-60">

                    <!-- Pagination -->
                    <nav class="g-mb-100" aria-label="Page Navigation" style="text-align:center" id="stickyblock-end">
                        {{ $products->appends($_GET)->links('frontend.partials.paginate') }}
                    </nav>
                    <!-- End Pagination -->
                </div>
            </div>
            <!-- End Content -->

            <!-- Filters -->
            <div class="col-md-3 order-md-1 g-brd-right--lg g-brd-gray-light-v4 g-pt-30">
                <form action="{{ route('shop') }}" id="stickyblock-start" class="FilterForm">
                    @if(request()->has('search'))
                        <input type="hidden" name='search' value="{{ request()->get('search') }}" />
                    @endif

                    @if(request()->has('filter'))
                        <input type="hidden" name='filter' value="{{ request()->get('filter') }}" />
                    @endif

                    <div class="g-pr-15--lg g-pt-30 js-sticky-block g-sticky-block--lg" data-type="responsive" data-start-point="#stickyblock-start" data-end-point="#stickyblock-end">
                        <!-- Categories -->
                        <div class="g-mb-30">
                            <h3 class="h5 my-3">Categories</h3>
                            @php
                                $parameters = request()->input();
                                $parameters['page'] = 1;
                            @endphp
                            <ul class="list-unstyled" id="categories" nav-parent="0">
                                
                                @if($categories->first())
                                    @foreach ($categories->first()->subcategories as $c)
                                        <li class="g-color-gray-dark-v4 inner-list show" nav-parent="0" >
                                            @php
                                                $parameters['filter'] = $c->slug;
                                            @endphp
                                            <label class="u-check">
                                                <a href="{{ route('shop',$parameters) }}" class="{{ $c->slug == Request::get('filter') ? 'g-color-primary':'g-color-gray-dark-v4' }} u-check-icon-checkbox-v4 g-brd-none g-color-primary--hover" style="font-size: 14px; text-decoration:none;">
                                                    {{ $c->subcat_name }}
                                                </a>
                                            </label>


                                        </li>
                                    @endforeach
                                @endif
                            </ul>

                        </div>
                        <!-- End Categories -->

                        <hr>
                        
                        <!-- Pricing -->
                        <div class="g-mb-30">
                            <h3 class="h5 mb-3">Pricing</h3>
                            @php
                                $min = 1;
                                $max = \App\Product::max('regular_price')                               
                            @endphp
                            <div class="text-center">
                                <span class="d-block g-color-primary mb-4">Rs.(<span id="rangeSliderAmount3">0</span>)</span>
                                <div id="rangeSlider1" class="u-slider-v1-3"
                                    data-result-container="rangeSliderAmount3"
                                    data-range="true"
                                    data-default="{{ request()->input('price.min', $min).','. request()->input('price.max', $max) }}"
                                    data-min="1"
                                    data-max="{{ $max }}">
                                </div>
                                <input name="price[min]" id="priceMin" type="hidden">
                                <input name="price[max]" id="priceMax" type="hidden">
                            </div>
                        </div>
                        <!-- End Pricing -->

                        <hr>

                        <!-- Brand -->
                        <div class="g-mb-30">
                        <h3 class="h5 mb-3">Brand</h3>

                        <ul class="list-unstyled">
                            @foreach ($brands as $brand)
                                <li class="my-2">
                                    <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                        <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" name="brands[]" value="{{ $brand->slug }}" {{ in_array($brand->slug,request()->input('brands',[])) ? 'checked' : '' }}>
                                        <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                            <i class="fa" data-check-icon="&#xf00c"></i>
                                        </span>
                                        {{ $brand->name }} <span class="float-right g-font-size-13">{{ $brand->products()->count() }}</span>
                                    </label>
                                </li>
                            @endforeach
                            
                        </ul>
                        </div>
                        <!-- End Brand -->

                        <hr>

                        <button class="btn btn-block u-btn-black g-font-size-12 text-uppercase g-py-12 g-px-25" type="submit" id="FilterProduct">Filter</button>
                    </div>
                </form>
            </div>
            <!-- End Filters -->
        </div>
    </div>
    <!-- End Products -->
@endsection

@section('js')
<script src="/frontend-assets/main-assets/assets/vendor/jquery-ui/ui/widget.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/jquery-ui/ui/widgets/menu.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/jquery-ui/ui/widgets/mouse.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/jquery-ui/ui/widgets/slider.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.slider.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.sticky-block.js"></script>
<script>
    // initialization of range slider
    $.HSCore.components.HSSlider.init('#rangeSlider1');
    // initialization of sticky blocks
    setTimeout(function() {
        $.HSCore.components.HSStickyBlock.init('.js-sticky-block');
    }, 1);

    
    $(document).ready(function(){
        $('.addToCart').on('click',function(e){
            var id = $(this).data('id');
            var ele = $(this);
            ele.attr('disabled','disabled');
            ele.find('i').removeClass('icon-finance-100').addClass('fa fa-spinner fa-spin');

            $.ajax({
                type: "POST",
                url: "{{ URL::to('/cart/additem') }}",
                data: { id:id, _token:'{{ csrf_token() }}', quantity: 1 }, // serializes the form's elements.
                success: function(data)
                {
                    ele.removeAttr('disabled');
                    ele.find('i').addClass('icon-finance-100').removeClass('fa fa-spinner fa-spin');

                    themeNotify('success','Item Successfully added to Cart.');
                },
                error:function(response)
                {
                    ele.removeAttr('disabled');
                    ele.find('i').addClass('icon-finance-100').removeClass('fa fa-spinner fa-spin');

                    showErrors(response);
                }
            });
        });

        $('.FilterForm').on('submit',function(e){
            e.preventDefault();

            var value = $( "#rangeSlider1" ).slider( "values" );
            $('#priceMin').val(value[0]);
            $('#priceMax').val(value[1]);

            this.submit();
        })
    })

    
</script>

@endsection