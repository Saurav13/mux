@extends('layouts.main') 
@section('body')
@section('title','| Services')
<section class="g-bg-size-cover g-bg-pos-center g-bg-cover g-bg-black-opacity-0_5--after g-color-white g-py-50 g-mb-20" style="background-image: url({{asset('frontend/assets/img-temp/stock/background1.jpg')}});">
    <div class="container g-bg-cover__inner">
      <header class="g-mb-20">
        <h2 class="h1 g-font-weight-300 text-uppercase">{{$category->cat_name}}
        </h2>
      </header>
  
      <ul class="u-list-inline">
        <li class="list-inline-item g-mr-7">
          <a class="u-link-v5 g-color-white g-color-primary--hover" href="#!">Home</a>
          <i class="fa fa-angle-right g-ml-7"></i>
        </li>
        <li class="list-inline-item g-color-primary">
        <span>{{$category->cat_name}}</span>
        </li>
      </ul>
    </div>
  </section>

        <div class="container">
            <div class="row">
              @if(count($subcategories) >0 )

                <div class="col-md-3 g-pt-15 g-pb-20">
                  <!-- Nav tabs -->
                  <ul class="nav flex-column u-nav-v8-1" role="tablist" data-target="nav-8-1-primary-ver" data-tabs-mobile-type="slide-up-down" data-btn-classes="btn btn-md btn-block rounded-0 u-btn-outline-primary">
                    @foreach($subcategories as $sub)
                    <li class="nav-item">
                    <a class="nav-link 
                      @if(Request::is('category'.'/'.$category->slug.'/'.$sub->slug)) 
                        active 
                      @elseif(Request::is('category'.'/'.$category->slug))
                        @if($loop->first)
                          active
                        @endif
                      @endif
                    "  
                    data-toggle="tab" href="#{{$sub->slug}}" role="tab">
                        {{-- <span>
                          <img class="u-nav-v8__icon u-icon-v3 u-icon-size--lg g-rounded-50x g-brd-around g-brd-4 g-brd-white" src="{{asset('subcategory-images/150x150'.'/'.$sub->featured_image)}}"/>
                        </span> --}}
                      <strong class="text-uppercase u-nav-v8__title">{{$sub->subcat_name}}</strong>
                      </a>
                    </li>
                    @endforeach
                  </ul>
                  <!-- End Nav tabs -->
                </div>
              
                <div class="col-md-9">
                  <!-- Tab panes -->
                  <div id="nav-8-1-primary-ver" class="tab-content g-pt-20 g-pt-md--0">
                      @foreach($subcategories as $sub)
                        <div class="tab-pane fade show 
                        @if(Request::is('category'.'/'.$category->slug.'/'.$sub->slug)) 
                        active 
                        @elseif(Request::is('category'.'/'.$category->slug))
                        @if($loop->first)
                        active
                        @endif
                        @endif
                        " id="{{$sub->slug}}" role="tabpanel">
                          <!-- Products Block -->
                            <div class="row">
                              @if(count($products->where('subcat_id','=',$sub->id)) != null)
                              @foreach($products->where('subcat_id','=',$sub->id) as $p)
                              <div class="col-sm-6 col-md-4 g-mb-30">
                                <!-- Article -->
                                <article class="g-pos-rel g-rounded-4 g-brd-bottom g-brd-3 g-brd-gray-light-v4 g-brd-primary--hover text-center g-transition-0_3 g-transition--linear">
                                    @foreach($images->where('product_id','=',$p->id)->slice(0,1) as $img)
                                  <!-- Article Image -->
                                  <figure>
                                    <img class="w-100" src="{{asset('product-images/750x500'.'/'.$img->image)}}" alt="{{$p->product_name}}"> 
                                    <!-- End Image Caption -->
                                  </figure>
                                  <!-- End Article Image -->
                                  @endforeach
                            
                                  <!-- Article Content -->
                                  <div class="g-bg-secondary g-pa-10">
                                    <h3 class="h4">
                                    <a class="g-color-main g-color-primary--hover g-text-underline--none--hover" href="{{URL::to('products'.'/'.$p->slug)}}">{{$p->product_name}}</a>
                                    </h3>
                                  <a class="btn btn-md btn-block u-btn-primary g-font-weight-600 g-font-size-13 text-uppercase g-py-12" href="{{URL::to('order'.'/'.$p->slug)}}">
                                      <i class="g-color-white g-mr-5 fa fa-heart"></i>
                                      Order
                                    </a>
                                  </div>
                                  <!-- End Article Content -->
                                </article>
                                <!-- End Article -->
                              </div>
                              @endforeach
                              @else
                              <div class="row">
                                <div class="g-pa-20 text-center">
                                <h2>No Products to display</h2>
                                </div>
                              </div>
                              @endif
                          </div>
                                        <!-- End Products Block -->
                        </div>
                      @endforeach
                    
                  </div>
                  <!-- End Tab panes -->
                </div>
              @else
                  @if(count($products->where('cat_id','=',$category->id)) > 0)
                    @foreach($products->where('cat_id','=',$category->id) as $p)
                      <div class="col-sm-6 col-md-4 g-mb-30">
                        <!-- Article -->
                        <article class="g-pos-rel g-rounded-4 g-brd-bottom g-brd-3 g-brd-gray-light-v4 g-brd-primary--hover text-center g-transition-0_3 g-transition--linear">
                            @foreach($images->where('product_id','=',$p->id)->slice(0,1) as $img)
                          <!-- Article Image -->
                          <figure>
                            <img class="w-100" src="{{asset('product-images/750x500'.'/'.$img->image)}}" alt="{{$p->product_name}}"> 
                            <!-- End Image Caption -->
                          </figure>
                          <!-- End Article Image -->
                          @endforeach
                    
                          <!-- Article Content -->
                          <div class="g-bg-secondary g-pa-10">
                            <h3 class="h4">
                            <a class="g-color-main g-color-primary--hover g-text-underline--none--hover" href="{{URL::to('products'.'/'.$p->slug)}}">{{$p->product_name}}</a>
                            </h3>
                          <a class="btn btn-md btn-block u-btn-primary g-font-weight-600 g-font-size-13 text-uppercase g-py-12" href="{{URL::to('order'.'/'.$p->slug)}}">
                              <i class="g-color-white g-mr-5 fa fa-heart"></i>
                              Order
                            </a>
                          </div>
                          <!-- End Article Content -->
                        </article>
                        <!-- End Article -->
                      </div>
                    @endforeach
                  @else
                    
                    <div class="g-pa-20 text-center">
                      <h2>No Products to display</h2>
                    </div>
                    
                  @endif
              @endif
            </div>
        </div>



    @section('js')
    <!-- JS Unify -->
    <script src="{{asset('frontend/assets/js/components/hs.tabs.js')}}"></script>

    <!-- JS Plugins Init. -->
    <script>
            $(document).on('ready', function () {
                // initialization of tabs
                $.HSCore.components.HSTabs.init('[role="tablist"]');
            });

            $(window).on('resize', function () {
                setTimeout(function () {
                    $.HSCore.components.HSTabs.init('[role="tablist"]');
                }, 200);
            });
    </script> @endsection @endsection