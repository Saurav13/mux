@extends('layouts.main')

@section('title','| Shop')

@section('body')

  <style>
    .item_price{
      color: #555;
      font-size: 14px;
      line-height: 11px;
    }
    .item_title{
      color: #555;
      font-size: 14px;
    }
    button:focus {
        outline: none;
    }
  </style>
  <div class="row align-items-stretch">
      <div class="col-lg-12">
          <!-- Article -->
          <article class="text-center g-color-white g-overflow-hidden">
              <div class="g-min-height-200 g-flex-middle g-bg-cover g-bg-size-cover g-bg-bluegray-opacity-0_2--after g-transition-0_5" data-bg-img-src="/img/banner.jpg" style="background-image: url(/img/banner.jpg);">
                  <div class="g-flex-middle-item g-pos-rel g-z-index-1 g-pb-50 g-pt-50 g-px-20">
                      {{-- <h3 class="page_title">Accessories</h3> --}}
                  </div>
              </div>
          </article>
          <!-- End Article -->
      </div>
          
          
  </div>
   <!-- Products -->
      <div class="container">
        <div class="row">
          <!-- Content -->
          <div class="col-md-9 order-md-2">
            <div class="g-pl-15--lg">
              <!-- Filters -->
              <div class="d-flex justify-content-end align-items-center g-brd-bottom g-brd-gray-light-v4 g-pt-40 g-pb-20">
            

                <!-- Sort By -->
                <div class="g-mr-60">
                  <h2 class="h6 align-middle d-inline-block g-font-weight-400 text-uppercase g-pos-rel g-top-1 mb-0">Sort by:</h2>

                  <!-- Secondary Button -->
                  <div class="d-inline-block btn-group g-line-height-1_2">
                    <button type="button" class="btn btn-secondary dropdown-toggle h6 align-middle g-brd-none g-color-gray-dark-v5 g-color-black--hover g-bg-transparent text-uppercase g-font-weight-300 g-font-size-12 g-pa-0 g-pl-10 g-ma-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Price
                    </button>
                    <div class="dropdown-menu rounded-0">
                      <a class="dropdown-item g-color-gray-dark-v4 g-font-weight-300" href="#!">Price low to high</a>
                      <a class="dropdown-item g-color-gray-dark-v4 g-font-weight-300" href="#!">price high to low</a>
                    </div>
                  </div>
                  <!-- End Secondary Button -->
                </div>
                <!-- End Sort By -->

               
              </div>
              <!-- End Filters -->

              <!-- Products -->
              <div class="row">
                @foreach ($products as $item)
                  <div class="col-md-6 col-lg-3 g-pt-20 g-mb-30">
                    <!-- Article -->
                    <article class="u-block-hover u-block-hover--uncroped text-center">
                      <!-- Article Image -->
                      <figure class="g-pos-rel g-mb-10">
                        <a class="d-block u-block-hover__additional--jump" href="{{URL::to('products'.'/'.$item->slug)}}">
                          <img class="w-100" src="{{ $item->firstImage }}" alt="{{ $item->product_name }}">
                          
                        </a>
                        @if($item->featured)
                            <figcaption class="w-100 g-bg-primary g-bg-black--hover text-center g-pos-abs g-bottom-0 g-transition-0_2 g-py-5">
                                <a class="g-color-white g-font-size-11 text-uppercase g-letter-spacing-1 g-text-underline--none--hover" href="javascript:void(0)">Featured</a>
                            </figcaption>
                        @endif
                      </figure>
                      <!-- End Article Image -->
                
                      <!-- Article Info -->
                      <em class="d-block g-font-style-normal item_price g-mb-0">Rs. {{ number_format($item->price) }}</em>
                      {{-- <div class="js-rating g-color-primary g-font-size-11 g-mb-10" data-rating="3.5" data-spacing="5"></div> --}}
                      <h3 class="h5">
                        <a class=" g-text-underline--none--hover item_title" href="{{URL::to('products'.'/'.$item->slug)}}">{{ $item->product_name }}</a>
                      </h3>
                      <!-- End Article Info -->

                      <!-- Products Icons -->
                      <ul class="list-inline media-body">
                              
                          <li class="list-inline-item align-middle mx-0">
                              <button data-id="{{ $item->id }}" class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle addToCart"
                                  style="background-color: transparent;border: none;cursor:pointer"
                                  data-toggle="tooltip"
                                  data-placement="top"
                                  title="Place Order">
                                  <i class="icon-finance-100 u-line-icon-pro" style="margin-top:3px"></i>
                              </button>
                          </li>
                      </ul>
                      <!-- End Products Icons -->
                    </article>
                    <!-- End Article -->
                  </div>
                @endforeach
              </div>
              <!-- End Products -->

              <hr class="g-mb-60">

              <!-- Pagination -->
              <nav class="g-mb-100" aria-label="Page Navigation" style="text-align:center" id="stickyblock-end">
                  {{ $products->appends($_GET)->links('frontend.partials.paginate') }}
              </nav>
              <!-- End Pagination -->
            </div>
          </div>
          <!-- End Content -->

          <!-- Filters -->
          <div class="col-md-3 order-md-1 g-brd-right--lg g-brd-gray-light-v4 g-pt-30">

            <form action="{{ route('shop') }}" id="stickyblock-start" class="FilterForm">
                @if(request()->has('search'))
                    <input type="hidden" name='search' value="{{ request()->get('search') }}" />
                @endif

                @if(request()->has('filter'))
                    <input type="hidden" name='filter' value="{{ request()->get('filter') }}" />
                @endif

                <div class="g-pr-15--lg g-pt-30 js-sticky-block g-sticky-block--lg" data-type="responsive" data-start-point="#stickyblock-start" data-end-point="#stickyblock-end">
                  <!-- Categories -->
                  <div class="g-mb-30">
                    <h3 class="h5 mb-3">Categories</h3>

                    <ul class="list-unstyled">
                      
                      @if($categories->first())
                        @foreach ($categories->first()->subcategories as $c)
                          <li class="my-3">
                              @php
                                  $parameters['filter'] = $c->slug;
                              @endphp
                              <a href="{{ route('shop',$parameters) }}" class="d-block u-link-v5 {{ $c->slug == Request::get('filter') ? 'g-color-primary':'g-color-gray-dark-v4' }} g-color-primary--hover">
                                  {{ $c->subcat_name }}
                                  <span class="float-right g-font-size-12">{{ $c->products()->count() }}</span></a>
                              </a>
                          </li>
                        @endforeach
                      @endif
                    </ul>
                  </div>
                  <!-- End Categories -->

                  <hr>

                <!-- Pricing -->
                <div class="g-mb-30">
                  <h3 class="h5 mb-3">Pricing</h3>

                  @php
                      $min = 1;
                      $max = \App\Product::max('regular_price')                               
                  @endphp
                  <div class="text-center">
                      <span class="d-block g-color-primary mb-4">Rs.(<span id="rangeSliderAmount3">0</span>)</span>
                      <div id="rangeSlider1" class="u-slider-v1-3"
                          data-result-container="rangeSliderAmount3"
                          data-range="true"
                          data-default="{{ request()->input('price.min', $min).','. request()->input('price.max', $max) }}"
                          data-min="1"
                          data-max="{{ $max }}">
                      </div>
                      <input name="price[min]" id="priceMin" type="hidden">
                      <input name="price[max]" id="priceMax" type="hidden">
                  </div>
                </div>
                <!-- End Pricing -->

                <hr>

                <!-- Brand -->
                <div class="g-mb-30">
                  <h3 class="h5 mb-3">Brand</h3>

                  <ul class="list-unstyled">
                    @foreach ($brands as $brand)
                      <li class="my-2">
                          <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                              <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" name="brands[]" value="{{ $brand->slug }}" {{ in_array($brand->slug,request()->input('brands',[])) ? 'checked' : '' }}>
                              <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                  <i class="fa" data-check-icon="&#xf00c"></i>
                              </span>
                              {{ $brand->name }} <span class="float-right g-font-size-13">{{ $brand->products()->count() }}</span>
                          </label>
                      </li>
                    @endforeach
                  </ul>
                </div>
                <!-- End Brand -->

                <hr>

                <button class="btn btn-block u-btn-black g-font-size-12 text-uppercase g-py-12 g-px-25" type="submit">Filter</button>
                <br><br>
              </div>
            </form>
          </div>
          <!-- End Filters -->
        </div>
      </div>
      <!-- End Products -->

      <!-- Call to Action -->
      <div class="g-bg-primary">
        <div class="container g-py-20">
          <div class="row justify-content-center">
            <div class="col-md-4 mx-auto g-py-20">
              <!-- Media -->
              <div class="media g-px-50--lg">
                <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-real-estate-048 u-line-icon-pro"></i>
                <div class="media-body">
                  <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free Shipping</span>
                  <span class="d-block g-color-white-opacity-0_8">In 2-3 Days</span>
                </div>
              </div>
              <!-- End Media -->
            </div>

            <div class="col-md-4 mx-auto g-brd-x--md g-brd-white-opacity-0_3 g-py-20">
              <!-- Media -->
              <div class="media g-px-50--lg">
                <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-real-estate-040 u-line-icon-pro"></i>
                <div class="media-body">
                  <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free Returns</span>
                  <span class="d-block g-color-white-opacity-0_8">No Questions Asked</span>
                </div>
              </div>
              <!-- End Media -->
            </div>

            <div class="col-md-4 mx-auto g-py-20">
              <!-- Media -->
              <div class="media g-px-50--lg">
                <i class="d-flex g-color-white g-font-size-40 g-pos-rel g-top-3 mr-4 icon-hotel-restaurant-062 u-line-icon-pro"></i>
                <div class="media-body text-left">
                  <span class="d-block g-color-white g-font-weight-500 g-font-size-17 text-uppercase">Free 24</span>
                  <span class="d-block g-color-white-opacity-0_8">Days Storage</span>
                </div>
              </div>
              <!-- End Media -->
            </div>
          </div>
        </div>
      </div>
      <!-- End Call to Action -->

@endsection


@section('js')
<script src="/frontend-assets/main-assets/assets/vendor/jquery-ui/ui/widget.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/jquery-ui/ui/widgets/menu.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/jquery-ui/ui/widgets/mouse.js"></script>
<script src="/frontend-assets/main-assets/assets/vendor/jquery-ui/ui/widgets/slider.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.slider.js"></script>
<script src="/frontend-assets/main-assets/assets/js/components/hs.sticky-block.js"></script>
<script>
    $(document).ready(function(){
      $.HSCore.components.HSSlider.init('#rangeSlider1');

      setTimeout(function() {
          $.HSCore.components.HSStickyBlock.init('.js-sticky-block');
      }, 1);


      $('.addToCart').on('click',function(e){
          var id = $(this).data('id');
          var ele = $(this);
          ele.attr('disabled','disabled');
          ele.find('i').removeClass('icon-finance-100').addClass('fa fa-spinner fa-spin');

          $.ajax({
              type: "POST",
              url: "{{ URL::to('/cart/additem') }}",
              data: { id:id, _token:'{{ csrf_token() }}', quantity: 1 }, // serializes the form's elements.
              success: function(data)
              {
                  ele.removeAttr('disabled');
                  ele.find('i').addClass('icon-finance-100').removeClass('fa fa-spinner fa-spin');

                  $('#cartTotal').text(data.count);
                  themeNotify('success','Item Successfully added to Cart.');
              },
              error:function(response)
              {
                  ele.removeAttr('disabled');
                  ele.find('i').addClass('icon-finance-100').removeClass('fa fa-spinner fa-spin');

                  showErrors(response);
              }
          });
      });

      $('.FilterForm').on('submit',function(e){
          e.preventDefault();

          var value = $( "#rangeSlider1" ).slider( "values" );
          $('#priceMin').val(value[0]);
          $('#priceMax').val(value[1]);

          this.submit();
      })
    });
</script>
@endsection