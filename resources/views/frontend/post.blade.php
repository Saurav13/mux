@extends('layouts.main')
@section('title','| '.$post->title)

@section('meta-tags')
    <meta property="og:url"           content="{{ url()->current() }}" />
    <meta property="og:type"          content="article" />
    <meta property="og:title"         content="{{ $post->title }}" />
    <meta property="article:author"   content="{{ $post->author }}" />
    <meta property="article:published_time"   content="{{ date(DATE_ISO8601, strtotime($post->updated_at)) }}" />
    <meta property="og:description"   content="{!! substr(strip_tags($post->content),0,200) !!}..." />
    <meta property="og:image"         content="{{ URL::to('blog_images/'.$post->image) }}" />
@endsection

@section('body')

    <!-- Blog Single Item Banner -->
    <section class="g-bg-cover g-bg-size-cover g-bg-white-gradient-opacity-v1--after" data-bg-img-src="{{ route('optimize', ['blog_images',$post->image,1920,1080]) }}">
        <div class="container text-center g-pos-rel g-z-index-1 g-pb-50">
        <div class="row d-flex justify-content-center align-content-end flex-wrap g-min-height-500">
            <div class="col-lg-7 mt-auto">
            <div class="mb-5">
                <h1 class="g-color-white g-font-weight-600 g-mb-30">{{ $post->title }}</h1>
            </div>
            <span class="g-color-white-opacity-0_7 g-font-weight-300 g-font-size-12">{{ $post->author }}</span>
            <span class="g-color-white-opacity-0_7 g-font-weight-700 mx-3">&#183;</span>
            <span class="g-color-white-opacity-0_7 g-font-weight-300 g-font-size-12">{{ date('M j, Y',strtotime($post->updated_at)) }}</span>
            </div>
        </div>
        </div>
    </section>
    <!-- End Blog Single Item Banner -->

    <!-- Blog Single Item Info -->
    <section class="container g-pt-100 g-pb-60">
        <div class="row justify-content-center">
            <div class="col-lg-9">
                {!! $post->content !!}
            </div>
        </div>
    </section>
    <!-- End Blog Single Item Info -->

    <!-- Blog Single Item Author -->
    <section class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9">

                <!-- Social Icons -->
                <div class="text-center">
                    <h3 class="h6 g-color-black g-font-weight-600 text-uppercase mb-3">Share:</h3>
                    <ul class="list-inline g-mb-60 social">
                        <li class="list-inline-item g-mx-2">
                            <a class="u-icon-v1 u-icon-slide-up--hover g-color-gray-dark-v4 g-color-facebook--hover social-share" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(Request::fullUrl()) }}">
                                <i class="g-font-size-default g-line-height-1 u-icon__elem-regular fa fa-facebook"></i>
                                <i class="g-font-size-default g-line-height-0_8 u-icon__elem-hover fa fa-facebook"></i>
                            </a>
                        </li>
                        <li class="list-inline-item g-mx-2">
                            <a class="u-icon-v1 u-icon-slide-up--hover g-color-gray-dark-v4 g-color-twitter--hover social-share" href="https://twitter.com/intent/tweet?url={{ urlencode(Request::fullUrl()) }}">
                                <i class="g-font-size-default g-line-height-1 u-icon__elem-regular fa fa-twitter"></i>
                                <i class="g-font-size-default g-line-height-0_8 u-icon__elem-hover fa fa-twitter"></i>
                            </a>
                        </li>
                        <li class="list-inline-item g-mx-2">
                            <a class="u-icon-v1 u-icon-slide-up--hover g-color-gray-dark-v4 g-color-linkedin--hover social-share" href="https://www.linkedin.com/shareArticle?mini=true&url={{ urlencode(Request::fullUrl()) }}">
                                <i class="g-font-size-default g-line-height-1 u-icon__elem-regular fa fa-linkedin"></i>
                                <i class="g-font-size-default g-line-height-0_8 u-icon__elem-hover fa fa-linkedin"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End Social Icons -->

            </div>
        </div>
    </section>
    <!-- End Blog Single Item Author -->

@endsection

@section('js')
<script>
    var popupMeta = {
      width: 400,
      height: 400
    }
    $('.social').on('click', '.social-share', function(event){
        event.preventDefault();
    
        var vPosition = Math.floor(($(window).width() - popupMeta.width) / 2),
            hPosition = Math.floor(($(window).height() - popupMeta.height) / 2);
        
        var url = $(this).attr('href');
        var popup = window.open(url, 'Social Share',
            'width='+popupMeta.width+',height='+popupMeta.height+
            ',left='+vPosition+',top='+hPosition+
            ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');
    
        if (popup) {
            popup.focus();
            return false;
        }
    });
</script>
@endsection