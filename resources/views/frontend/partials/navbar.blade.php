<body>
  <main>

    <style>
      .nav_link{
        font-weight: 600;
        font-size: 13px;
      }
    </style>
    <!-- Header -->
    <header id="js-header" class="u-header u-header--static u-shadow-v19">
      <!-- Top Bar -->
      <div class="u-header__section g-brd-bottom g-brd-gray-light-v4 g-bg-black g-transition-0_3">
        <div class="container">
          <div class="row justify-content-between align-items-center g-mx-0--lg">
            <div class="col-sm-auto g-hidden-sm-down">
              <!-- Social Icons -->
              <ul class="list-inline g-py-14 mb-0">
                <li class="list-inline-item">
                  <a class="g-color-white-opacity-0_8 g-color-primary--hover g-pa-3" href="#!">
                    <i class="fa fa-facebook"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a class="g-color-white-opacity-0_8 g-color-primary--hover g-pa-3" href="#!">
                    <i class="fa fa-twitter"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a class="g-color-white-opacity-0_8 g-color-primary--hover g-pa-3" href="#!">
                    <i class="fa fa-tumblr"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a class="g-color-white-opacity-0_8 g-color-primary--hover g-pa-3" href="#!">
                    <i class="fa fa-pinterest-p"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a class="g-color-white-opacity-0_8 g-color-primary--hover g-pa-3" href="#!">
                    <i class="fa fa-google"></i>
                  </a>
                </li>
              </ul>
              <!-- End Social Icons -->
            </div>

            <div class="col-sm-auto g-hidden-sm-down g-color-white-opacity-0_6 g-font-weight-400 g-pl-15 g-pl-0--sm g-py-14">
              <i class="icon-communication-163 u-line-icon-pro g-font-size-18 g-valign-middle g-color-white-opacity-0_8 g-mr-10 g-mt-minus-2"></i>
              8 800 1234 4321
            </div>

            <div class="col-sm-auto g-pos-rel g-py-14">
              <!-- List -->
              <ul class="list-inline g-overflow-hidden g-pt-1 g-mx-minus-4 mb-0">
                <li class="list-inline-item g-mx-4">
                  <a class="g-color-white-opacity-0_6 g-color-primary--hover g-font-weight-400 g-text-underline--none--hover" href="page-our-stores-1.html">Our Stores</a>
                </li>

                <li class="list-inline-item g-color-white-opacity-0_3 g-mx-4">|</li>
                <li class="list-inline-item g-mx-4">
                  <a class="g-color-white-opacity-0_6 g-color-primary--hover g-font-weight-400 g-text-underline--none--hover" href="page-help-1.html">Help</a>
                </li>

                <li class="list-inline-item g-color-white-opacity-0_3 g-mx-4">|</li>
                <!-- Account -->
                <li class="list-inline-item">
                  <a id="account-dropdown-invoker-2" class="g-color-white-opacity-0_6 g-color-primary--hover g-font-weight-400 g-text-underline--none--hover" href="#!"
                     aria-controls="account-dropdown-2"
                     aria-haspopup="true"
                     aria-expanded="false"
                     data-dropdown-event="hover"
                     data-dropdown-target="#account-dropdown-2"
                     data-dropdown-type="css-animation"
                     data-dropdown-duration="300"
                     data-dropdown-hide-on-scroll="false"
                     data-dropdown-animation-in="fadeIn"
                     data-dropdown-animation-out="fadeOut">
                    Account
                  </a>
                  <ul id="account-dropdown-2" class="list-unstyled u-shadow-v29 g-pos-abs g-bg-white g-width-160 g-pb-5 g-mt-19 g-z-index-2"
                      aria-labelledby="account-dropdown-invoker-2">
                    @guest
                      <li>
                        <a class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20" href="/login?next={{ urlencode(url()->current()) }}">
                          Login
                        </a>
                      </li>
                      <li>
                        <a class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20" href="/register?next={{ urlencode(url()->current()) }}">
                          Signup
                        </a>
                      </li>
                    @else
                      <li>
                        <a class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20" href="page-orders-1.html">
                          Your Orders
                        </a>
                      </li>
                      <li>
                        <a class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20" href="/logout" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                          Logout
                        </a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                        </form>
                      </li>
                    @endguest
                  </ul>
                </li>
                <!-- End Account -->
              </ul>
              <!-- End List -->
            </div>

            <div class="col-sm-auto g-pr-15 g-pr-0--sm">
              <!-- Basket -->
              <div class="u-basket d-inline-block g-z-index-3">
                <div class="g-py-10 g-px-6">
                  <a href="/cart" id="basket-bar-invoker" class="u-icon-v1 g-color-white-opacity-0_8 g-color-primary--hover g-font-size-17 g-text-underline--none--hover"
                     aria-controls="basket-bar"
                     aria-haspopup="true"
                     aria-expanded="false"
                     data-dropdown-event="hover"
                     data-dropdown-target="#basket-bar"
                     data-dropdown-type="css-animation"
                     data-dropdown-duration="300"
                     data-dropdown-hide-on-scroll="false"
                     data-dropdown-animation-in="fadeIn"
                     data-dropdown-animation-out="fadeOut">
                    <span class="u-badge-v1--sm g-color-white g-bg-primary g-font-size-11 g-line-height-1_4 g-rounded-50x g-pa-4" style="top: 7px !important; right: 3px !important;" id="cartTotal">{{ Cart::content()->count() }}</span>
                    <i class="icon-hotel-restaurant-105 u-line-icon-pro"></i>
                  </a>
                </div>

              </div>
              <!-- End Basket -->

              <!-- Search -->
              <div class="d-inline-block g-valign-middle">
                <div class="g-py-10 g-pl-15">
                  <a href="#!" class="g-color-white-opacity-0_8 g-color-primary--hover g-font-size-17 g-text-underline--none--hover"
                     aria-haspopup="true"
                     aria-expanded="false"
                     data-dropdown-event="hover"
                     aria-controls="searchform-1"
                     data-dropdown-target="#searchform-1"
                     data-dropdown-type="css-animation"
                     data-dropdown-duration="300"
                     data-dropdown-animation-in="fadeInUp"
                     data-dropdown-animation-out="fadeOutDown">
                    <i class="g-pos-rel g-top-3 icon-education-045 u-line-icon-pro"></i>
                  </a>
                </div>
                <!-- Search Form -->
                <form id="searchform-1" action="/shop" class="u-searchform-v1 u-dropdown--css-animation u-dropdown--hidden u-shadow-v20 g-brd-around g-brd-gray-light-v4 g-bg-white g-right-0 rounded g-pa-10 1g-mt-8">
                  <div class="input-group">
                    <input class="form-control g-font-size-13 w-100" type="search" name="search" placeholder="Search Here..." required>
                    <div class="input-group-btn p-0">
                      <button class="btn u-btn-primary g-font-size-12 text-uppercase g-py-13 g-px-15" type="submit">Go</button>
                    </div>
                  </div>
                </form>
                <!-- End Search Form -->
              </div>
              <!-- End Search -->
            </div>
          </div>
        </div>
      </div>
      <!-- End Top Bar -->

      <div class="u-header__section u-header__section--light g-bg-white g-transition-0_3">
        <nav class="js-mega-menu navbar navbar-expand-lg">
          <div class="container">
            <!-- Responsive Toggle Button -->
            <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-3 g-right-0" type="button"
              aria-label="Toggle navigation"
              aria-expanded="false"
              aria-controls="navBar"
              data-toggle="collapse"
              data-target="#navBar">
              <span class="hamburger hamburger--slider g-pr-0">
                <span class="hamburger-box">
                  <span class="hamburger-inner"></span>
                </span>
              </span>
            </button>
            <!-- End Responsive Toggle Button -->

            <!-- Logo -->
            <a class="navbar-brand" href="home-page-1.html">
              <img style="height:4rem;" src="/logo.png" alt="Image Description">
            </a>
            <!-- End Logo -->

            <!-- Navigation -->
            <div id="navBar" class="collapse navbar-collapse align-items-center flex-sm-row g-pt-15 g-pt-0--lg">
              <ul class="navbar-nav ml-auto">
                <!-- Home - Submenu -->
                <li class="nav-item g-mx-10--lg g-mx-15--xl">
                  <a id="nav-link--home" class="nav-link nav_link text-uppercase g-color-primary--hover g-px-5 g-py-20" href="#!">
                    Home
                  </a>

                </li>
                <!-- End Home - Submenu -->

                <!-- Pages - Submenu -->
                <li class="nav-item hs-has-sub-menu g-mx-10--lg g-mx-15--xl">
                  <a id="nav-link--pages" class="nav-link nav_link text-uppercase g-color-primary--hover g-px-5 g-py-20" href="#!"
                     aria-haspopup="true"
                     aria-expanded="false"
                     aria-controls="nav-submenu--pages">
                    Shop Accessories
                  </a>

                  <!-- Submenu -->
                  <ul class="hs-sub-menu list-unstyled u-shadow-v11 g-min-width-220 g-brd-top g-brd-primary g-brd-top-2 g-mt-10" id="nav-submenu--pages"
                      aria-labelledby="nav-link--pages">
                    <!-- Grid Filter -->
                    <li class="dropdown-item hs-has-sub-menu">
                      <a id="nav-link--pages--grid-filter" class="nav-link g-color-gray-dark-v4" href="#!"
                         aria-haspopup="true"
                         aria-expanded="false"
                         aria-controls="nav-submenu--pages--grid-filter">
                        Laptop Accessories
                      </a>

                      <!-- Submenu (level 2) -->
                      <ul id="nav-submenu--pages--grid-filter" class="hs-sub-menu list-unstyled u-shadow-v11 g-min-width-220 g-brd-top g-brd-primary g-brd-top-2 g-mt-minus-2"
                          aria-labelledby="nav-link--pages--grid-filter">
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-grid-filter-left-sidebar-1.html">Left Sidebar</a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-grid-filter-right-sidebar-1.html">Right Sidebar
                            {{-- <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span> --}}
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-grid-filter-no-sidebar-1.html">No Sidebar 1
                            {{-- <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span> --}}
                          </a>
                        </li>
                      </ul>
                      <!-- End Submenu (level 2) -->
                    </li>
                    <!-- Grid Filter -->

                    <!-- List Filter -->
                    <li class="dropdown-item hs-has-sub-menu">
                      <a id="nav-link--pages--list-filter" class="nav-link g-color-gray-dark-v4" href="#!"
                         aria-haspopup="true"
                         aria-expanded="false"
                         aria-controls="nav-submenu--pages--list-filter">
                        Mobile Accessories
                      </a>

                      <!-- Submenu (level 2) -->
                      <ul id="nav-submenu--pages--list-filter" class="hs-sub-menu list-unstyled u-shadow-v11 g-brd-top g-brd-primary g-brd-top-2 g-min-width-220 g-mt-minus-2"
                          aria-labelledby="nav-link--pages--list-filter">
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-list-filter-left-sidebar-1.html">Left Sidebar</a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-list-filter-right-sidebar-1.html">Right Sidebar
                            {{-- <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span> --}}
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-list-filter-no-sidebar-1.html">No Sidebar 1
                            {{-- <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span> --}}
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-list-filter-no-sidebar-2.html">No Sidebar 2
                            {{-- <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span> --}}
                          </a>
                        </li>
                      </ul>
                      <!-- End Submenu (level 2) -->
                    </li>
                    <!-- List Filter -->

                    <!-- Left/Right Category -->
                    <li class="dropdown-item hs-has-sub-menu">
                      <a id="nav-link--pages--left-right-category" class="nav-link g-color-gray-dark-v4" href="#!"
                         aria-haspopup="true"
                         aria-expanded="false"
                         aria-controls="nav-submenu--pages--left-right-category">
                        Desktop Accessories
                      </a>

                      <!-- Submenu (level 2) -->
                      <ul id="nav-submenu--pages--left-right-category" class="hs-sub-menu list-unstyled u-shadow-v11 g-brd-top g-brd-primary g-brd-top-2 g-min-width-220 g-mt-minus-2"
                          aria-labelledby="nav-link--pages--left-right-category">
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-category-left-sidebar-1.html">Left Sidebar
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-category-right-sidebar-1.html">Right Sidebar
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-category-no-sidebar-1.html">No Sidebar 1
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-category-no-sidebar-2.html">No Sidebar 2
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-category-no-sidebar-3.html">No Sidebar 3
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-category-no-sidebar-4.html">No Sidebar 4
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                      </ul>
                      <!-- End Submenu (level 2) -->
                    </li>
                    <!-- Left/Right Category -->

             
             

                 
                  </ul>
                  <!-- End Submenu -->
                </li>
                <!-- End Pages - Submenu -->

                <li class="nav-item hs-has-sub-menu g-mx-10--lg g-mx-15--xl">
                  <a id="nav-link--pages" class="nav-link nav_link text-uppercase g-color-primary--hover g-px-5 g-py-20" href="#!"
                     aria-haspopup="true"
                     aria-expanded="false"
                     aria-controls="nav-submenu--pages">
                    Shop Parts
                  </a>

                  <!-- Submenu -->
                  <ul class="hs-sub-menu list-unstyled u-shadow-v11 g-min-width-220 g-brd-top g-brd-primary g-brd-top-2 g-mt-10" id="nav-submenu--pages"
                      aria-labelledby="nav-link--pages">
                    <!-- Grid Filter -->
                    <li class="dropdown-item hs-has-sub-menu">
                      <a id="nav-link--pages--grid-filter" class="nav-link g-color-gray-dark-v4" href="#!"
                         aria-haspopup="true"
                         aria-expanded="false"
                         aria-controls="nav-submenu--pages--grid-filter">
                        Laptop Parts
                      </a>

                      <!-- Submenu (level 2) -->
                      <ul id="nav-submenu--pages--grid-filter" class="hs-sub-menu list-unstyled u-shadow-v11 g-min-width-220 g-brd-top g-brd-primary g-brd-top-2 g-mt-minus-2"
                          aria-labelledby="nav-link--pages--grid-filter">
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-grid-filter-left-sidebar-1.html">Left Sidebar</a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-grid-filter-right-sidebar-1.html">Right Sidebar
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-grid-filter-no-sidebar-1.html">No Sidebar 1
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                      </ul>
                      <!-- End Submenu (level 2) -->
                    </li>
                    <!-- Grid Filter -->

                    <!-- List Filter -->
                    <li class="dropdown-item hs-has-sub-menu">
                      <a id="nav-link--pages--list-filter" class="nav-link g-color-gray-dark-v4" href="#!"
                         aria-haspopup="true"
                         aria-expanded="false"
                         aria-controls="nav-submenu--pages--list-filter">
                        Mobile Parts
                      </a>

                      <!-- Submenu (level 2) -->
                      <ul id="nav-submenu--pages--list-filter" class="hs-sub-menu list-unstyled u-shadow-v11 g-brd-top g-brd-primary g-brd-top-2 g-min-width-220 g-mt-minus-2"
                          aria-labelledby="nav-link--pages--list-filter">
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-list-filter-left-sidebar-1.html">Left Sidebar</a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-list-filter-right-sidebar-1.html">Right Sidebar
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-list-filter-no-sidebar-1.html">No Sidebar 1
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-list-filter-no-sidebar-2.html">No Sidebar 2
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-list-filter-no-sidebar-3.html">No Sidebar 3
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-list-filter-no-sidebar-4.html">No Sidebar 4
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                      </ul>
                      <!-- End Submenu (level 2) -->
                    </li>
                    <!-- List Filter -->

                    <!-- Left/Right Category -->
                    <li class="dropdown-item hs-has-sub-menu">
                      <a id="nav-link--pages--left-right-category" class="nav-link g-color-gray-dark-v4" href="#!"
                         aria-haspopup="true"
                         aria-expanded="false"
                         aria-controls="nav-submenu--pages--left-right-category">
                        Desktop Parts
                      </a>

                      <!-- Submenu (level 2) -->
                      <ul id="nav-submenu--pages--left-right-category" class="hs-sub-menu list-unstyled u-shadow-v11 g-brd-top g-brd-primary g-brd-top-2 g-min-width-220 g-mt-minus-2"
                          aria-labelledby="nav-link--pages--left-right-category">
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-category-left-sidebar-1.html">Left Sidebar
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-category-right-sidebar-1.html">Right Sidebar
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-category-no-sidebar-1.html">No Sidebar 1
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-category-no-sidebar-2.html">No Sidebar 2
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-category-no-sidebar-3.html">No Sidebar 3
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                        <li class="dropdown-item">
                          <a class="nav-link g-color-gray-dark-v4" href="page-category-no-sidebar-4.html">No Sidebar 4
                            <span class="u-label g-rounded-3 g-font-size-10 g-bg-lightred g-py-3 g-pos-rel g-top-minus-1 g-ml-5">New</span>
                          </a>
                        </li>
                      </ul>
                      <!-- End Submenu (level 2) -->
                    </li>
                    <!-- Left/Right Category -->


                 
                  </ul>
                  <!-- End Submenu -->
                </li>
               
              
              
        
                <li class="nav-item g-ml-10--lg">
                  <a class="nav-link nav_link text-uppercase g-color-primary--hover g-pl-5 g-pr-0 g-py-20" href="/">About Us</a>
                </li>
             

                <li class="nav-item g-ml-10--lg">
                  <a class="nav-link nav_link text-uppercase g-color-primary--hover g-pl-5 g-pr-0 g-py-20" href="/">Contact</a>
                </li>
              </ul>
            </div>
            <!-- End Navigation -->
          </div>
        </nav>
      </div>
    </header>
    <!-- End Header -->
