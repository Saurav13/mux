<div id="contacts-section" class="g-bg-black-opacity-0_9 g-color-white-opacity-0_8 g-py-60">
    <div class="container">
      <div class="row">
        <!-- Footer Content -->
        <div class="col-lg-4 col-md-7 g-mb-40 g-mb-0--lg">
          <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
            <h2 class="u-heading-v2__title h6 text-uppercase mb-0">About Us</h2>
          </div>

          <p>About Unify dolor sit amet, consectetur adipiscing elit. Maecenas eget nisl id libero tincidunt sodales.</p>
        </div>
        <!-- End Footer Content -->

        <!-- Footer Content -->
        

        <!-- Footer Content -->
        <div class="col-lg-4 col-md-7 g-mb-40 g-mb-0--lg">
          <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
            <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Useful Links</h2>
          </div>

          <nav class="text-uppercase1">
            <ul class="list-unstyled g-mt-minus-10 mb-0">
              <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                <h4 class="h6 g-pr-20 mb-0">
              <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#!">About Us</a>
              <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
            </h4>
              </li>
              <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                <h4 class="h6 g-pr-20 mb-0">
              <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#!">Portfolio</a>
              <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
            </h4>
              </li>
              <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                <h4 class="h6 g-pr-20 mb-0">
              <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#!">Our Services</a>
              <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
            </h4>
              </li>
              <li class="g-pos-rel g-brd-bottom g-brd-white-opacity-0_1 g-py-10">
                <h4 class="h6 g-pr-20 mb-0">
              <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#!">Latest Jobs</a>
              <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
            </h4>
              </li>
              <li class="g-pos-rel g-py-10">
                <h4 class="h6 g-pr-20 mb-0">
              <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#!">Contact Us</a>
              <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
            </h4>
              </li>
            </ul>
          </nav>
        </div>
        <!-- End Footer Content -->

        <!-- Footer Content -->
        <div class="col-lg-4 col-md-7">
          <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
            <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Our Contacts</h2>
          </div>

          <address class="g-bg-no-repeat g-font-size-12 mb-0" style="background-image: url(/frontend-assets/main-assets/assets/img/maps/map2.png);">
        <!-- Location -->
        <div class="d-flex g-mb-20">
          <div class="g-mr-10">
            <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
              <i class="fa fa-map-marker"></i>
            </span>
          </div>
          <p class="mb-0">Hattisar
             <br>Kathmandu, Nepal</p>
        </div>
        <!-- End Location -->

        <!-- Phone -->
        <div class="d-flex g-mb-20">
          <div class="g-mr-10">
            <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
              <i class="fa fa-phone"></i>
            </span>
          </div>
          <p class="mb-0">01-4442548 <br> (+123) 456 7891</p>
        </div>
        <!-- End Phone -->

        <!-- Email and Website -->
        <div class="d-flex g-mb-20">
          <div class="g-mr-10">
            <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
              <i class="fa fa-globe"></i>
            </span>
          </div>
          <p class="mb-0">
            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="mailto:info@htmlstream.com">info@htmlstream.com</a>
            <br>
            <a class="g-color-white-opacity-0_8 g-color-white--hover" href="#!">www.htmlstream.com</a>
          </p>
        </div>
        <!-- End Email and Website -->
      </address>
        </div>
        <!-- End Footer Content -->
      </div>
    </div>
  </div>
  <!-- End Footer -->
  <!-- End Copyright Footer -->
  <a class="js-go-to u-go-to-v1" href="#!" data-type="fixed" data-position='{
   "bottom": 15,
   "right": 15
 }' data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn">
    <i class="hs-icon hs-icon-arrow-top"></i>
  </a>
</main>


  <div style="position: fixed; bottom: 0; right: 0;z-index:3" id="alertStack"></div>

<div id="messageModal" class="text-left g-max-width-600 g-bg-primary g-overflow-y-auto g-pa-20" style="display: none; max-width:1000px !important;background-color: white!important;">
    <button type="button" class="close" onclick="Custombox.modal.close();">
      <i class="hs-icon hs-icon-close"></i>
    </button>
    
    <div class="container">
          <div class="text-center">
              <h4 class="g-mb-20" id="alerttitle"></h4>
              <p><strong id="alertcontent"></strong></p>
             <button class="btn btn-neutral"  onclick="Custombox.modal.close();">Close</button>
          </div>
     </div>
</div>
<!-- JS Global Compulsory -->
<script src="{{asset('frontend-assets/main-assets/assets/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('frontend-assets/main-assets/assets/vendor/jquery-migrate/jquery-migrate.min.js')}}"></script>
<script src="{{asset('frontend-assets/main-assets/assets/vendor/popper.min.js')}}"></script>
<script src="{{asset('frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.js')}}"></script>

<script src="/frontend-assets/main-assets/assets/vendor/appear.js"></script>

<script src="{{asset('frontend-assets/main-assets/assets/vendor/bootstrap/offcanvas.js')}}"></script>
<script src="{{asset('frontend-assets/main-assets/assets/vendor/hs-megamenu/src/hs.megamenu.js')}}"></script>
<script src="{{asset('frontend-assets/main-assets/assets/vendor/slick-carousel/slick/slick.js')}}"></script>
<!-- JS Unify -->
<script src="{{asset('frontend-assets/main-assets/assets/js/hs.core.js')}}"></script>
<script src="{{asset('frontend-assets/main-assets/assets/js/components/hs.header.js')}}"></script>
<script src="{{asset('frontend-assets/main-assets/assets/js/helpers/hs.hamburgers.js')}}"></script>
<script src="/frontend-assets/main-assets/assets/js/helpers/hs.height-calc.js"></script>

<script src="{{asset('frontend-assets/main-assets/assets/js/components/hs.dropdown.js')}}"></script>
<script src="{{asset('frontend-assets/main-assets/assets/js/components/hs.carousel.js')}}"></script>
<script src="{{asset('frontend-assets/main-assets/assets/js/components/hs.go-to.js')}}"></script>

<!-- JS Unify -->
<script  src="/frontend-assets/main-assets/assets/vendor/custombox/custombox.min.js"></script>
<script  src="/frontend-assets/main-assets/assets/js/components/hs.modal-window.js"></script>
<!-- JS Unify -->
<script src="{{asset('frontend-assets/main-assets/assets/js/custom.js')}}"></script>
<script src="{{asset('/js/main.js')}}"></script>

@yield('js')

<!-- JS Plugins Init. -->
<script>
  $(document).on('ready', function () {
    // initialization of go to
      // initialization of header
    $.HSCore.components.HSHeader.init($('#js-header'));
    $.HSCore.helpers.HSHamburgers.init('.hamburger');
    $.HSCore.components.HSGoTo.init('.js-go-to');

    // initialization of carousel
    // $.HSCore.components.HSCarousel.init('.js-carousel');

    // initialization of HSDropdown component
    $.HSCore.helpers.HSHeightCalc.init();

    $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
      afterOpen: function(){
        $(this).find('input[type="search"]').focus();
      }
    });

  });


     $(window).on('load', function () {
      // initialization of header
      $.HSCore.components.HSHeader.init($('#js-header'));
      $.HSCore.helpers.HSHamburgers.init('.hamburger');

      
      // initialization of HSMegaMenu component
      $('.js-mega-menu').HSMegaMenu();
    });
</script>
</body>

</html>