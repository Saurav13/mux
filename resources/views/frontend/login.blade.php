@extends('layouts.main')


@section('body')
<section class="overflow-hidden">
        <div class="row no-gutters">
          <div class="col-lg-6" style="background: url(/img/signup.jpg); background-size:contain">
            <!-- Promo Block - Slider -->
            
            <!-- End Promo Block - Slider -->
          </div>
  
          <div class="col-lg-6">
            <div class="g-pa-40 g-mx-70--xl">
              <!-- Form -->
              <form class="g-py-15">
                <h2 class="h3 g-color-black mb-4">Login</h2>
  
                <div class="mb-4">
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15" type="email" placeholder="email">
                </div>
                <div class="mb-4">
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15" type="password" placeholder="password">
                </div>
  
               
  
                <div class="g-mb-30">
                  <button class="btn btn-md btn-block u-btn-primary rounded text-uppercase g-py-13" type="button">Signup</button>
                </div>
  
                
                <p class="g-font-size-13 text-center mb-0">SignUp as User? <a class="g-font-weight-600" href="/register">Signup</a>
                </p>
                <p class="g-font-size-13 text-center mb-0">SignUp as Wholesellers? <a class="g-font-weight-600" href="/agent-register">Signup</a>
                </p>
              </form>
              <!-- End Form -->
            </div>
          </div>
        </div>
      </section>    
@endsection