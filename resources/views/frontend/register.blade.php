@extends('layouts.main')


@section('body')
<section class="overflow-hidden">
        <div class="row no-gutters">
          <div class="col-lg-6" style="background: url(/img/signup.jpg); background-size:contain">
            <!-- Promo Block - Slider -->
            
            <!-- End Promo Block - Slider -->
          </div>
  
          <div class="col-lg-6">
            <div class="g-pa-40 g-mx-70--xl">
              <!-- Form -->
              <form class="g-py-15">
                <h2 class="h3 g-color-black mb-4">Signup</h2>
  

                <div class="mb-4">
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15" type="text" placeholder="First Name">
                </div>

                <div class="mb-4">
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15" type="text" placeholder="Last Name">
                </div>
                <div class="mb-4">
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15" type="email" placeholder="email">
                </div>
                <div class="mb-4">
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15" type="password" placeholder="password">
                </div>
  
               
  
                <div class="mb-1">
                  <label class="form-check-inline u-check g-font-size-13 g-pl-25 mb-2">
                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                    <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                      <i class="fa g-rounded-2" data-check-icon=""></i>
                    </div>
                    I accept the <a href="#">Terms and Conditions</a>
                  </label>
                </div>
  
                <div class="mb-3">
                  <label class="form-check-inline u-check g-font-size-13 g-pl-25 mb-2">
                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                    <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                      <i class="fa g-rounded-2" data-check-icon=""></i>
                    </div>
                    Subscribe to our newsletter
                  </label>
                </div>
  
                <div class="g-mb-30">
                  <button class="btn btn-md btn-block u-btn-primary rounded text-uppercase g-py-13" type="button">Signup</button>
                </div>
  
                <p class="g-font-size-13 text-center mb-0">Already have an account? <a class="g-font-weight-600" href="page-signup-11.html">Login</a>
                </p>
                <p class="g-font-size-13 text-center mb-0">Signup as wholesalers? <a class="g-font-weight-600" href="/agent-register">Signup</a>
                </p>
              </form>
              <!-- End Form -->
            </div>
          </div>
        </div>
      </section>    
@endsection