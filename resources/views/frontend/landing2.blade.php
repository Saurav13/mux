@extends('layouts.main')
@section('css')
   <!-- Revolution Slider -->
   <link rel="stylesheet" href="{{asset('frontend/assets/vendor/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}">
   <link rel="stylesheet" href="{{asset('frontend/assets/vendor/revolution-slider/revolution/css/settings.css') }}">
   <link rel="stylesheet" href="{{asset('frontend/assets/vendor/revolution-slider/revolution/css/layers.css') }}">
   <link rel="stylesheet" href="{{asset('frontend/assets/vendor/revolution-slider/revolution/css/navigation.css') }}">
   <link rel="stylesheet" href="{{asset('frontend/assets/vendor/revolution-slider/revolution-addons/typewriter/css/typewriter.css') }}">

@endsection

@section('body')

  <!-- Revolution Slider -->
  <div class="g-overflow-hidden">
    <div id="rev_slider_1014_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="typewriter-effect" data-source="gallery" style="background-color:transparent;padding:0px;">
      <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
      <div id="rev_slider_1014_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.1">
        <ul>
          @foreach ($landing_images as $img)
            <!-- SLIDE  -->
            <li data-index="rs-2800" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="/frontend/assets/img-temp/img2.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
              <!-- MAIN IMAGE -->
              <img src="/landing_images/{{ $img->image }}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgparallax="10" class="rev-slidebg">
              <!-- LAYERS -->
              <!-- LAYER NR. 1 -->
              <div class="tp-caption tp-shape tp-shapewrapper"
                  id="slide-2800-layer-7"
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                  data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                  data-width="full"
                  data-height="full"
                  data-whitespace="nowrap"

                  data-type="shape"
                  data-basealign="slide"
                  data-responsive_offset="off"
                  data-responsive="off"
                  data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"speed":5000,"to":"opacity:0;","ease":"Power4.easeInOut"}]'
                  data-textAlign="['left','left','left','left']"
                  data-paddingtop="[0,0,0,0]"
                  data-paddingright="[0,0,0,0]"
                  data-paddingbottom="[0,0,0,0]"
                  data-paddingleft="[0,0,0,0]"

                  style="z-index: 5;background-color:rgba(0, 0, 0, 0.50);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>

              <!-- LAYER NR. 2 -->
              {{-- <div class="tp-caption   tp-resizeme"
                  id="slide-2800-layer-1"
                  data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                  data-y="['middle','middle','middle','middle']" data-voffset="['-160','-160','-130','-157']"
                  data-fontsize="['110','110','100','60']"
                  data-lineheight="['110','110','100','60']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"

                  data-type="text"
                  data-responsive_offset="on"

                  data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                  data-textAlign="['center','center','center','center']"
                  data-paddingtop="[0,0,0,0]"
                  data-paddingright="[0,0,0,0]"
                  data-paddingbottom="[0,0,0,0]"
                  data-paddingleft="[0,0,0,0]"

                  style="z-index: 6; white-space: nowrap; font-size: 110px; line-height: 110px; font-weight: 700; color: rgba(255, 255, 255, 1.00);border-width:0px;letter-spacing:-7px;">Summer Collection
              </div> --}}
            </li>
            <!-- END SLIDE  -->
          @endforeach

        </ul>
        <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
      </div>
    </div>
  </div>
  <!-- End Revolution Slider -->

  <!-- Features -->
  <div class="g-brd-bottom g-brd-gray-light-v4">
    <div class="container g-py-30">
      <div class="row justify-content-center">
        <div class="col-md-4 mx-auto g-py-15">
          <!-- Media -->
          <div class="media g-px-50--lg">
            <i class="d-flex g-color-black g-font-size-30 g-pos-rel g-top-3 mr-4 icon-real-estate-048 u-line-icon-pro"></i>
            <div class="media-body">
              <span class="d-block g-font-weight-500 g-font-size-default text-uppercase">Free Shipping</span>
              <span class="d-block g-color-gray-dark-v4">In 2-3 Days</span>
            </div>
          </div>
          <!-- End Media -->
        </div>

        <div class="col-md-4 mx-auto g-brd-x--md g-brd-gray-light-v3 g-py-15">
          <!-- Media -->
          <div class="media g-px-50--lg">
            <i class="d-flex g-color-black g-font-size-30 g-pos-rel g-top-3 mr-4 icon-real-estate-040 u-line-icon-pro"></i>
            <div class="media-body">
              <span class="d-block g-font-weight-500 g-font-size-default text-uppercase">Free Returns</span>
              <span class="d-block g-color-gray-dark-v4">No Questions Asked</span>
            </div>
          </div>
          <!-- End Media -->
        </div>

        <!-- Media -->
        <div class="col-md-4 mx-auto g-py-15">
          <div class="media g-px-50--lg">
            <i class="d-flex g-color-black g-font-size-30 g-pos-rel g-top-3 mr-4 icon-hotel-restaurant-062 u-line-icon-pro"></i>
            <div class="media-body text-left">
              <span class="d-block g-font-weight-500 g-font-size-default text-uppercase">Free 24</span>
              <span class="d-block g-color-gray-dark-v4">Days Storage</span>
            </div>
          </div>
          <!-- End Media -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Features -->

  <!-- Categories -->
  <div class="container g-pt-100 g-pb-70">
    <div class="row g-mx-minus-10">
      <div class="col-sm-6 col-md-4 g-px-10 g-mb-30">
        <article class="u-block-hover">
          <img class="w-100 u-block-hover__main--zoom-v1 g-mb-minus-8" src="/frontend/assets/img-temp/img1.jpg" alt="Image Description">
          <div class="g-pos-abs g-bottom-30 g-left-30">
            <span class="d-block g-color-black">Collections</span>
            <h2 class="h1 mb-0">Women</h2>
          </div>
          <a class="u-link-v2" href="#!"></a>
        </article>
      </div>

      <div class="col-sm-6 col-md-4 g-px-10 g-mb-30">
        <article class="u-block-hover">
          <img class="w-100 u-block-hover__main--zoom-v1 g-mb-minus-8" src="/frontend/assets/img-temp/img1.jpg" alt="Image Description">
          <div class="g-pos-abs g-bottom-30 g-left-30">
            <span class="d-block g-color-black">Collections</span>
            <h2 class="h1 mb-0">Children</h2>
          </div>
          <a class="u-link-v2" href="#!"></a>
        </article>
      </div>

      <div class="col-sm-6 col-md-4 g-px-10 g-mb-30">
        <article class="u-block-hover">
          <img class="w-100 u-block-hover__main--zoom-v1 g-mb-minus-8" src="/frontend/assets/img-temp/img1.jpg" alt="Image Description">
          <div class="g-pos-abs g-bottom-30 g-left-30">
            <span class="d-block g-color-black">Collections</span>
            <h2 class="h1 mb-0">Men</h2>
          </div>
          <a class="u-link-v2" href="#!"></a>
        </article>
      </div>
    </div>
  </div>
  <!-- End Categories -->

  <!-- Icon Blocks -->
  @if(count($featured_products) > 0)
    <div class="container g-pb-70">
      <div class="text-center mx-auto g-max-width-600 g-mb-50">
        <h2 class="g-color-black mb-4">Featured Products</h2>
        <p class="lead">We want to create a range of beautiful, practical and modern outerwear that doesn't cost the earth – but let's you still live life in style.</p>
      </div>

      <div id="carouselCus1" class="js-carousel g-pb-100 g-mx-minus-10"
          data-infinite="true"
          data-slides-show="4"
          data-lazy-load="ondemand"
          data-arrows-classes="u-arrow-v1 g-pos-abs g-bottom-0 g-width-45 g-height-45 g-color-gray-dark-v5 g-bg-secondary g-color-white--hover g-bg-primary--hover rounded"
          data-arrow-left-classes="fa fa-angle-left g-left-10"
          data-arrow-right-classes="fa fa-angle-right g-right-10"
          data-pagi-classes="u-carousel-indicators-v1 g-absolute-centered--x g-bottom-20 text-center">
          @foreach($featured_products as $p)
            <div class="js-slide">
              <div class="g-px-10">
                <!-- Product -->
                @foreach($images->where('product_id',$p->id)->slice(0,1) as $i)
                  <figure class="g-pos-rel g-mb-20">
                    <img class="img-fluid" data-lazy="{{route('optimize',['product-images*750x500',$i->image,480,700])}}" alt="Image Description">

                    <!-- <figcaption class="w-100 g-bg-primary g-bg-black--hover text-center g-pos-abs g-bottom-0 g-transition-0_2 g-py-5">
                      <a class="g-color-white g-font-size-11 text-uppercase g-letter-spacing-1 g-text-underline--none--hover" href="#!">New Arrival</a>
                    </figcaption> -->
                  </figure>
                @endforeach
                <div class="media">
                  <!-- Product Info -->
                  <div class="d-flex flex-column">
                    <h4 class="h6 g-color-black mb-1">
                      <a class="u-link-v5 g-color-black g-color-primary--hover" href="{{URL::to('products'.'/'.$p->slug)}}">
                        {{$p->product_name}}
                      </a>
                    </h4>
                    <a href="/wallpapers?filter={{$p->subcategory->slug}}" class="d-inline-block g-color-gray-dark-v5 g-font-size-13">{{$p->subcategory->subcat_name}}</a>
                    <span class="d-block g-color-black g-font-size-17">Rs.{{ number_format($p->price) }} per roll</span>
                  </div>
                  <!-- End Product Info -->

                  <!-- Products Icons -->
                  <ul class="list-inline media-body text-right">
                      <li class="list-inline-item align-middle mx-0">
                          <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle"
                              href="{{URL::to('visualizer?alias='.$p->slug.'&filter='.$p->subcategory->slug)}}"
                              data-toggle="tooltip"
                              data-placement="top"
                              title="View" target="_blank">
                                      <i class="fa fa-eye u-line-icon-pro"></i>
                              
                          </a>
                      </li>
                      <li class="list-inline-item align-middle mx-0">
                          <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle"
                              href="{{URL::to('order'.'/'.$p->slug)}}"
                              data-toggle="tooltip"
                              data-placement="top"
                              title="Place Order">
                              <i class="icon-finance-100 u-line-icon-pro"></i>
                          </a>
                      </li>
                  </ul>
                  <!-- End Products Icons -->
                </div>
                <!-- End Product -->
              </div>
            </div>
          @endforeach
      </div>
    </div>
  @endif
  <!-- End Icon Blocks -->

  <!-- News -->
  <div class="container g-pb-70">
    <div class="text-center mx-auto g-max-width-600 g-mb-50">
      <h2 class="g-color-black mb-4">Blog Posts</h2>
      <p class="lead">Keep in touch with the latest blogs &amp; posts.</p>
    </div>

    <div class="row g-mx-minus-10">
      @foreach ($posts as $post)
        <div class="col-sm-6 col-md-4 g-px-10 g-mb-30">
          <!-- Blog Background Overlay Blocks -->
          <article class="u-block-hover">
            <div class="g-bg-cover g-bg-white-gradient-opacity-v1--after">
              <img class="d-flex align-items-end u-block-hover__main--mover-down" src="{{ route('optimize', ['blog_images',$post->image,650,650]) }}" alt="Image Description">
            </div>
            <div class="u-block-hover__additional--partially-slide-up text-center g-z-index-1 mt-auto">
              <div class="u-block-hover__visible g-pa-25">
                <h2 class="h4 g-color-white g-font-weight-400 mb-3">
                  <a class="u-link-v5 g-color-white g-color-primary--hover g-cursor-pointer" href="{{ route('blog',$post->slug) }}">{{ $post->title }}</a>
                </h2>
                <h4 class="d-inline-block g-color-white-opacity-0_7 g-font-size-11 mb-0">
                  By,
                  <span class="g-color-white-opacity-0_7 text-uppercase" >{{ $post->author }}</span>
                </h4>
                <span class="g-color-white-opacity-0_7 g-pos-rel g-top-2 mx-2">&#183;</span>
                <span class="g-color-white-opacity-0_7 g-font-size-10 text-uppercase">{{ date('M j, Y',strtotime($post->updated_at)) }}</span>
              </div>
  
              <a class="d-inline-block g-brd-bottom g-brd-white g-color-white g-font-weight-500 g-font-size-12 text-uppercase g-text-underline--none--hover g-mb-30" href="{{ route('blog',$post->slug) }}">Read more</a>
            </div>
          </article>
          <!-- End Blog Background Overlay Blocks -->
        
        </div>
      @endforeach
    </div>
  </div>
  <!-- End News -->
 
  <!-- Testimonials Advanced -->
  @if($testimonials->count() > 0)
    <section class="container g-py-50">
      <div class="row justify-content-center g-mb-20">
        <div class="col-lg-7">
          <!-- Heading -->
          <div class="text-center">
            <h2 class="h3 g-color-black text-uppercase mb-2">What people say</h2>
            <div class="d-inline-block g-width-35 g-height-2 g-bg-primary mb-2"></div>
          </div>
          <!-- End Heading -->
        </div>
      </div>

      <!-- Testimonials Advanced -->
      <div class="js-carousel" data-infinite="true" data-arrows-classes="u-arrow-v1 g-width-50 g-height-50 g-brd-1 g-brd-style-solid g-brd-gray-light-v1 g-brd-primary--hover g-color-gray-dark-v5 g-bg-primary--hover g-color-white--hover g-absolute-centered--y rounded-circle g-mt-minus-25"
      data-arrow-left-classes="fa fa-angle-left g-left-0" data-arrow-right-classes="fa fa-angle-right g-right-0">
      @foreach($testimonials as $testi)
        <div class="js-slide">
          <div class="row justify-content-center">
            <div class="col-lg-7">
              <!-- Testimonial Advanced -->
              <div class="g-pos-rel g-pt-20">
                <em class="g-color-gray-light-v5 g-font-size-120 g-pos-abs g-top-minus-15 g-left-minus-15 g-z-index-minus-1">
                    &#8220;
                  </em>
                <blockquote class="lead g-font-style-italic g-line-height-2 g-pl-30 g-mb-30">{{$testi->content}}</blockquote>

                <div class="media">
                <img class="d-flex align-self-center g-width-50 g-height-50 rounded-circle mr-3" src="{{asset('landing_images'.'/'.$testi->photo)}}" alt="{{$testi->photo}}">

                  <div class="media-body align-self-center">
                  <h4 class="h5 g-font-weight-700 g-mb-0">{{$testi->name}}</h4>
                  <span class="d-block g-color-gray-dark-v4">{{$testi->profession}}</span>
                  </div>
                </div>
              </div>
              <!-- End Testimonial Advanced -->
            </div>
          </div>
        </div>
        @endforeach
      </div>
      <!-- End Testimonials Advanced -->
    </section>
  @endif



  <!-- Call To Action -->
  <section class="g-bg-primary g-color-white g-pa-30" style="background-image: url({{asset('frontend/assets/img/bg/pattern5.png')}}">
    <div class="d-md-flex justify-content-md-center text-center">
      <div class="align-self-md-center">
        <p class="lead g-font-weight-400 g-mr-20--md g-mb-15 g-mb-0--md">Send Us your design and we will print it for you</p>
      </div>
      <div class="align-self-md-center">
      <a class="btn btn-lg u-btn-white text-uppercase g-font-weight-600 g-font-size-12" target="_blank" href="{{URL::to('contact')}}">Contact Us</a>
      </div>
    </div>
  </section>

  <!-- Demo modal window -->
{{-- <div id="modal-type-aftersometime" class="js-autonomous-popup text-left g-max-width-600 g-bg-white g-overflow-y-auto g-pa-20" style="display: none;" data-modal-type="aftersometime" data-open-effect="zoomIn" data-close-effect="zoomOut" data-delay="2000" data-speed="500">
  <button type="button" class="close" onclick="Custombox.modal.close();">
    <i class="hs-icon hs-icon-close"></i>
  </button>
  <h4>Holi Offer!</h4>
  <br>
  <div class="g-pb-10">
    <img class="img-fluid" src="{{asset('frontend/assets/img-temp/stock/offer.jpg')}}" style="height:300px; width:600px;" alt="Image Description">
  </div>
  <p>Here we can add any special offers (like holi offer or any discount offers) and anything new the company has to offer which will pop up after sometimes.</p>
  
</div> --}}
<!-- End Demo modal window -->
    
@endsection

@section('js')

  <script>
    $.HSCore.components.HSCarousel.init('[class*="js-carousel"]');

    $('#carouselCus1').slick('setOption', 'responsive', [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 4
        }
    }, {
      breakpoint: 992,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 768,
      settings: {
        slidesToShow: 2
      }
    }], true);
    
    // initialization of revolution slider
    var tpj = jQuery;

    var revapi1014;
    tpj(document).ready(function () {
      if (tpj("#rev_slider_1014_1").revolution == undefined) {
        revslider_showDoubleJqueryError("#rev_slider_1014_1");
      } else {
        revapi1014 = tpj("#rev_slider_1014_1").show().revolution({
          sliderType: "standard",
          jsFileLocation: "revolution/js/",
          sliderLayout: "fullscreen",
          dottedOverlay: "none",
          delay: 9000,
          navigation: {
            keyboardNavigation: "off",
            keyboard_direction: "horizontal",
            mouseScrollNavigation: "off",
            mouseScrollReverse: "default",
            onHoverStop: "off",
            touch: {
              touchenabled: "on",
              swipe_threshold: 75,
              swipe_min_touches: 1,
              swipe_direction: "horizontal",
              drag_block_vertical: false
            }
            ,
            arrows: {
              style: "uranus",
              enable: true,
              hide_onmobile: true,
              hide_under: 768,
              hide_onleave: false,
              tmp: '',
              left: {
                h_align: "left",
                v_align: "center",
                h_offset: 20,
                v_offset: 0
              },
              right: {
                h_align: "right",
                v_align: "center",
                h_offset: 20,
                v_offset: 0
              }
            }
          },
          parallax: {
            type: "mouse",
            origo: "slidercenter",
            speed: 2000,
            levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
            disable_onmobile: "on"
          },
          responsiveLevels: [1240, 1024, 778, 480],
          visibilityLevels: [1240, 1024, 778, 480],
          gridwidth: [1240, 1024, 778, 480],
          gridheight: [868, 768, 960, 600],
          lazyType: "none",
          shadow: 0,
          spinner: "off",
          stopLoop: "on",
          stopAfterLoops: 0,
          stopAtSlide: 1,
          shuffle: "off",
          autoHeight: "off",
          fullScreenAutoWidth: "off",
          fullScreenAlignForce: "off",
          fullScreenOffsetContainer: "",
          fullScreenOffset: "60px",
          disableProgressBar: "on",
          hideThumbsOnMobile: "off",
          hideSliderAtLimit: 0,
          hideCaptionAtLimit: 0,
          hideAllCaptionAtLilmit: 0,
          debugMode: false,
          fallbacks: {
            simplifyAll: "off",
            nextSlideOnWindowFocus: "off",
            disableFocusListener: false
          }
        });
      }

      RsTypewriterAddOn(tpj, revapi1014);
    });
  </script>
@endsection
