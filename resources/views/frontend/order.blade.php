@extends('layouts.main')
@section('body')
@section('title','| Order '.$product->product_name)
<style>
        .hide{
                display: none;
        }
</style>
<div class="container">			
        <div class="text-center">
                <img src="{{asset('product-images/180x180'.'/'.$image->image)}}" alt="Shrestha Digital Printers"  style="height:150px;" />
        <h3>Place your order for "{{$product->product_name}}"</h3>
      
        </div>
</div>	
<div  class="container">
        <form action="{{route('general.order')}}" method="POST" enctype="multipart/form-data" class="g-brd-around g-brd-gray-light-v4 g-pa-30 g-mb-30">
                {{csrf_field()}}
                <h3 class="g-mb-15">Order Information</h3>
                <p> *Note: If the following values do not match your requirement please specify your requirement in the message box below</p>
                <hr class="g-brd-gray-light-v4 g-mx-minus-30">

                <div class="row">
                        <div class="col-md-6">
                                <div class="form-group g-mb-20">
                                        <a href="javascript:void(0)" class="pull-right" id="notsure1">Not Sure?</a>
                        
                                        <label class="g-mb-10" for="autocomplete1"><h3>No of rolls</h3></label>
                                        <input id="quantity" class="form-control form-control-md rounded-0" type="number" placeholder="No of rolls"  min="1" name="order[quantity]" value="{{ old('order.quantity') ? old('order.quantity') : 1 }}" onchange="changeQuantity()">
                                </div>

                                <div class="mb-1 hide" id="notdivsure">
                                        <label class="form-check-inline u-check g-color-gray-dark-v5 g-font-size-14 g-pl-25 mb-2">
                                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" id="notsure2" name="order[notsure]" type="checkbox">
                                                <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                                                        <i class="fa" data-check-icon=""></i>
                                                </div>
                                                I am not sure about the measurements.
                                        </label>
                                </div>
                        </div>
                        <div class="col-md-6 hide" id="calculateBox">
                                <div class="row">
                                        <div class="col-md-4">
                                                <div class="form-group g-mb-5">
                                                        <label class="g-mb-5" for="autocomplete1">Height</label>
                                                        <input class="form-control form-control-md rounded-0" type="number" min="1" placeholder="Height" id="height">
                                        
                                                </div>
                                        </div>
                                        <div class="col-md-4">
                                                <div class="form-group g-mb-5">
                                                        <label class="g-mb-5" for="autocomplete1">Length</label>
                                                        <input class="form-control form-control-md rounded-0" type="number" min="1" placeholder="Length" id="length">
                                                        
                                                </div>
                                        </div>

                                        <div class="col-md-4">
                                                <div class="form-group g-mb-5">
                                                        <label class="g-mb-5" for="autocomplete1">Unit</label>
                                                        <select class="form-control form-control-md rounded-0" id="unit" style="height: 42px;">
                                                                <option value="feet" selected>Feet</option>
                                                                <option value="meter">Meters</option>
                                                        </select>
                                                </div>
                                        </div>
                                </div>
                                <button type="button" class="btn btn-xs btn-primary" onclick="calculateRoll()">Calculate</button>
                                <div class="row">
                                        <div class="col"><hr></div>
                                                <div class="col-auto" style="margin-top: 18px;color:#a7a7a7">OR</div>
                                        <div class="col"><hr></div>
                                </div>
                                <div class="row">
                                        <div class="col-md-4">
                                                <div class="form-group g-mb-5">
                                                        <label class="g-mb-5" for="autocomplete1">Area to cover</label>
                                                        <input class="form-control form-control-md rounded-0" type="number" min="1" placeholder="Area" id="area">
                                        
                                                </div>
                                        </div>
                                        <div class="col-md-4">
                                                <div class="form-group g-mb-5">
                                                        <label class="g-mb-5" for="autocomplete1">Unit</label>
                                                        <select class="form-control form-control-md rounded-0" id="unit1" style="height: 42px;">
                                                                <option value="feet" selected>Square Foot</option>
                                                                <option value="meter">Square Meter</option>
                                                        </select>
                                                </div>
                                        </div>
                                </div>
                                <button type="button" class="btn btn-xs btn-primary" onclick="calculateRollArea()">Calculate</button>
                        </div>
                </div>

                <div class="form-group g-mb-20">
                        <label class="g-mb-10" for="inputGroup1_1"><h3>Price per roll</h3></label>
                        <p>Rs. {{ $product->price }}</p>
                        
                </div>
                
                <div class="form-group g-mb-20">
                        <label class="g-mb-10" for="inputGroup1_1"><h3>Total Price</h3></label>
                        <p>Rs. <span id="price">{{ old('order.quantity') ? (old('order.quantity')*$product->price) : $product->price }}<span></p>
                        
                </div>
                <hr class="g-brd-gray-light-v4 g-mx-minus-30">
                
                <div class="row">
                    <div class="form-group col-md-6 g-mb-20">
                            <label class="g-mb-10" for="inputGroup1_1"><h3>Name*</h3></label>
                            <input id="inputGroup1_1" class="form-control form-control-md rounded-0" name="order[name]" value="{{ old('order.name') }}" type="text" placeholder="Enter name" required>
                            
                    </div>
                    
                    <div class="form-group col-md-6 g-mb-20">
                            <label class="g-mb-10" for="inputGroup1_1"><h3>Phone*</h3></label>
                            <input id="inputGroup1_1" class="form-control form-control-md rounded-0" type="text" placeholder="Enter a reachable phone number..." name="order[phone]"value="{{ old('order.phone') }}" required>
                            <small class="form-text text-muted g-font-size-default g-mt-10">We'll never share your number with anyone else.</small>
                    </div>
                    <div class="form-group col-md-6 g-mb-20">
                            <label class="g-mb-10" for="inputGroup1_1"><h3>Address*</h3></label>
                            <input id="inputGroup1_1" class="form-control form-control-md rounded-0" type="text" placeholder="Enter your address..." name="order[address]" value="{{ old('order.address') }}" required>
                    </div>
                    <div class="form-group col-md-6 g-mb-20">
                            <label class="g-mb-10" for="inputGroup1_1"><h3>Email</h3></label>
                            <input id="inputGroup1_1" class="form-control form-control-md rounded-0" name="order[email]" value="{{ old('order.email') }}" type="email" placeholder="Enter email">
                            <small class="form-text text-muted g-font-size-default g-mt-10">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group col-md-12 g-mb-20">
                            <label class="g-mb-10" for="inputGroup2_2"><h3>Message</h3></label>
                            <textarea id="inputGroup2_2" name="order[message]" class="form-control form-control-md rounded-0" rows="7" placeholder="A short description of what service you would like to use">{{ old('order.message') }}</textarea>
                    </div>
                </div>
                <input type="hidden" value="{{ $product->id }}" name="order[product_id]">

                <div class="form-group g-mb-20">
                        <input type="submit" class="btn btn-md btn-primary" value="Place Order" id="submit"/>
                </div>
        </form>
    
</div>
@section('js')

<script >
        function changeQuantity(){
                if(!$('#notsure2').prop('checked')){
                        if($('#quantity').val() < 1) {
                                $('#quantity').val(1);
                        }
                        $('#quantity').val(Math.ceil($('#quantity').val()));
                        $('#price').text($('#quantity').val()*{{ $product->price }});
                }
        }

        $("[id*='notsure']").on('click',function(){
                
                if($('#notsure2').prop('checked')){
                        $('#calculateBox').addClass('hide');
                        $('#quantity').attr('disabled','disabled');
                        $('#price').text({{ $product->price }});
                }else{
                        $('#calculateBox').removeClass('hide');
                        $('#notdivsure').removeClass('hide');
                        $('#quantity').removeAttr('disabled');
                        $('#price').text($('#quantity').val()*{{ $product->price }});  
                }
        });
        function calculateRoll(){
                if(!$('#notsure2').prop('checked')){
                        var unit = $('#unit').val();
                        var h = $('#height').val();
                        var l = $('#length').val();

                        if ($.isNumeric(h) && $.isNumeric(l)){
                                if(unit == 'meter'){
                                        h *= 3.281;
                                        l *= 3.281;
                                }
                                else if(unit != 'feet'){
                                        alert('wrong')
                                }
                                $('#quantity').val(Math.ceil((l*h)/27));
                                $('#price').text($('#quantity').val()*{{ $product->price }});
                        }
                }
                
        }

        function calculateRollArea(){
                if(!$('#notsure2').prop('checked')){
                        var unit = $('#unit1').val();
                        var a = $('#area').val();

                        if ($.isNumeric(a)){
                                if(unit == 'meter'){
                                        a *= 10.764;
                                }
                                else if(unit != 'feet'){
                                        alert('wrong')
                                }
                                $('#quantity').val(Math.ceil(a/27));
                                $('#price').text($('#quantity').val()*{{ $product->price }});
                        }
                }
        }
</script>


      


@endsection
@endsection