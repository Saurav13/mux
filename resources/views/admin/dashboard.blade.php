@extends('layouts.admin')

@section('body')
<div class="content-header row">
    </div>
    <div class="content-body">
        {{-- <div class="text-xs-right">
            @if($s == 'all')
                <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="#">See all unseen Orders</a>
            @else
                <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="#">See all orders</a>
            @endif
        </div> --}}
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">New Order Messages</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Ordered Product</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                {{-- @if(!$contact->seen && $state == 'all')
                                    <tr style="background:#d6d3d3">
                                @else
                                    <tr>
                                @endif --}}
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $order->name }}</td>
                                    <td>{{ $order->product_name }}</td>
                                    <td>
                                    <a class="btn btn-outline-info btn-sm" title="View" href="{{route('show.order',$order->id)}}"><i class="icon-eye"></i></a>
                                        <a id='deleteOrder{{$order->id}}' title="delete" type="button" class="btn btn-outline-danger btn-sm"><i class="icon-trash-o"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{-- @if(count($contacts)==0)
                            <p class="font-medium-3 text-muted text-xs-center" style="margin:100px">No {{ $state == 'all'?'':'New'}} Messages</p>
                        @endif --}}
                        <div class="text-xs-center mb-3">
                            <nav aria-label="Page navigation">
                                {{ $orders->links() }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('js')
<script>
    $(document).on('click',"[id*='deleteOrder']",function(event){
        var id = $(this).attr("id").slice(11);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
    
            showCancelButton: true,
    
          }).then(function(){
            $.post("{{route('delete.order')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
              swal({
                title:"Deleted Successfully",
                type:"success"
    
              }).then(function(){
               location.reload();
              })
            })
          });
    });
    </script>

@endsection