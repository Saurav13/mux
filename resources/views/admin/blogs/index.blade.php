@extends('layouts.admin')

@section('body')
   
   <div class="content-header row">
   </div>
   <input name="image" type="file" id="upload" class="hidden" onchange="">

   <div class="content-body">
       <div class="card">
           <div class="card-header">
               <h4 class="card-title"><a data-action="collapse">Add New Blog</a></h4>
               <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
               <div class="heading-elements">
                   <ul class="list-inline mb-0">
                       <li><a data-action="collapse"><i class="{{ count($errors)>0 ? 'icon-minus4':'icon-plus4' }}"></i></a></li>
                       <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                   </ul>
               </div>
           </div>
           <div class="card-body collapse {{ count($errors)>0 ? 'in':'out' }}">
               <div class="card-block card-dashboard">
                   <form class="form" method="POST" id="AddBlogForm" action="{{ route('blogs.store') }}" enctype="multipart/form-data">
                       {{ csrf_field() }}
                       <div class="form-body">
                            <h4 class="form-section"><i class="icon-eye6"></i> Blog Post</h4>

                            <div class="form-group">
                                <label for="title">Title</label>
                                <input class="form-control{{ $errors->has('title') ? ' border-danger' : '' }}" id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" required autofocus>

                                @if ($errors->has('title'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="userinput5">Author</label>
                                <input class="form-control{{ $errors->has('author') ? ' border-danger' : '' }}" type="text" placeholder="Author"  name="author" value="{{ old('author') }}" required>

                                @if ($errors->has('author'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('author') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Image</label>
                                <input class="form-control{{ $errors->has('image') ? ' border-danger' : '' }}" type="file" placeholder="Photo"  name="image" required>

                                @if ($errors->has('image'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Content</label>
                                @if ($errors->has('content'))
                                    <div class="alert alert-danger no-border mb-2">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </div>
                                @endif
                                <textarea cols="50" rows="20" class="form-control{{ $errors->has('content') ? ' border-danger' : '' }}" id="content" name="content">{{ old('content') }}</textarea>
                        
                            </div>
                       </div>

                       <div class="form-actions right">
                           <button type="submit" class="btn btn-primary">
                               <i class="icon-check2"></i> Post
                           </button>
                       </div>
                   </form>
               </div>
           </div>
       </div>

       <div class="card">
           <div class="card-header">
               <h4 class="card-title">Blog Posts</h4>
               <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
               <div class="heading-elements">
                   <ul class="list-inline mb-0">
                       <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                       <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                   </ul>
               </div>
           </div>
           <div class="card-body collapse in">
               <div class="card-block card-dashboard">
                   <div class="table-responsive">
                       <table class="table">
                           <thead>
                               <tr>
                                   <th>#</th>
                                   <th>Title</th>
                                   <th>Author</th>
                                   <th>Posted On</th>
                                   <th width="20%">Actions</th>
                               </tr>
                           </thead>
                           <tbody>
                               @foreach($blogs as $blog)
                               <tr>
                                   <td>{{ $loop->iteration + (($blogs->currentPage()-1) * $blogs->perPage()) }}</td>
                                   <td>{{ $blog->title }}</td>
                                   <td>{{ $blog->author }}</td>
                                   <td>
                                        {{ date('M j, Y',strtotime($blog->created_at)) }}
                                   </td>
                                   <td>
                                        <a class="btn btn-outline-primary" title="View Details" href="{{ route('blogs.show',$blog->id) }}"><i class="icon-eye"></i></a>

                                       <a class="btn btn-outline-warning" title="Update Details" href="{{ route('blogs.edit',$blog->id) }}"><i class="icon-edit"></i></a>
                                       
                                       <form action="{{ route('blogs.destroy',$blog->id) }}" method="POST" style="display:inline">
                                           {{ csrf_field() }}
                                           <input type="hidden" name="_method" value="DELETE" >
                                           <input type="hidden" name="page" value="{{$blogs->currentPage()}}"/>
                                           <button id='deleteBlog' type="button" class="btn btn-outline-danger"><i class="icon-trash-o"></i></button>
                                       </form>
                                   </td>
                               </tr>
                               @endforeach
                           </tbody>
                       </table>


                       <div class="text-xs-center mb-3">
                           <nav aria-label="Page navigation">
                               {{ $blogs->links() }}
                           </nav>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>

   
@endsection

@section('js')
   <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
   <script>
       tinymce.init({
           selector: "textarea#content",
           
           plugins: [
               "advlist autolink lists link image charmap print preview hr anchor pagebreak",
               "searchreplace wordcount visualblocks visualchars code",
               "insertdatetime media nonbreaking save table contextmenu directionality",
               "emoticons template paste textcolor colorpicker textpattern"
           ],
           toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
           toolbar2: "print preview | forecolor backcolor emoticons | template",
           image_advtab: true,
           file_picker_callback: function(callback, value, meta) {
           if (meta.filetype == 'image') {
               $('#upload').trigger('click');
               $('#upload').on('change', function() {
               var file = this.files[0];
               var reader = new FileReader();
               reader.onload = function(e) {
                   callback(e.target.result, {
                   alt: ''
                   });
               };
               reader.readAsDataURL(file);
               });
           }
           },
           templates: [
               {title: 'Newsletter1', description: 'Notice', url: "/templates/newsletter.html"}
             ]
       });

        $("[id*='delete']").click(function(e){
            var ele = this;
            e.preventDefault();
            
            swal({
            title: "Are you sure?",
            text: "You will not be able to recover this record!",
            type: "warning",

            showCancelButton: true,

            }).then(function(){
            ele.form.submit();
            }).catch(swal.noop);

        });
   </script>
  
@endsection