@extends('layouts.admin')

@section('body')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Post #{{ $blog->id }}</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ route('blogs.index') }}">Back</a>
                    <a class="btn btn-warning btn-min-width mr-1 mb-1 " href="{{ route('blogs.edit',$blog->id) }}">Edit</a>
                </ol>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section class="card">
            <div id="invoice-template" class="card-block">
                
                <div id="invoice-items-details" class="pt-2">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="text-xs-center">
                                <h2>{{ $blog->title }} </h2>
                                <p>- {{ $blog->author }}</p>
                            </div><br>
                            <div class="text-center" style="    text-align: center;">
                                <img class="img-fluid" src="{{ route('optimize', ['blog_images',$blog->image,290,290]) }}" alt="Card image cap">
                            </div>
                            <p>{!! $blog->content !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
