@extends('layouts.admin')
@section('body')

    <style>
        .switch input { 
            display:none;
        }
        .switch {
            display:inline-block;
            width:30px;
            height:15px;
            margin:8px;
            transform:translateY(50%);
            position:relative;
        }
        
        .slider {
            position:absolute;
            top:0;
            bottom:0;
            left:0;
            right:0;
            border-radius:15px;
            box-shadow:0 0 0 2px #777, 0 0 4px #777;
            cursor:pointer;
            border:4px solid transparent;
            overflow:hidden;
            transition:.4s;
        }
        .slider:before {
            position:absolute;
            content:"";
            width:100%;
            height:100%;
            background:#777;
            border-radius:15px;
            transform:translateX(-15px);
            transition:.4s;
        }
        
        input:checked + .slider:before {
            transform:translateX(15px);
            background:limeGreen;
        }
        input:checked + .slider {
            box-shadow:0 0 0 2px limeGreen,0 0 2px limeGreen;
        }
        
    </style>

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Manage Products</h2>
            <a class="btn btn-md btn-primary pull-right" href="{{ route('products.create') }}"><i class="icon-plus-circle"></i> Add Product</a>
        </div>

        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">

            <div class="breadcrumb-wrapper col-xs-12">
                @if(Request::is('admin/products'))
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{URL::to('admin/dashboard')}}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Manage Products
                        </li>
                    </ol>
                @else
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{URL::to('admin/dashboard')}}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{URL::to('admin/products')}}">Manage Products</a>
                        </li>
                        <li class="breadcrumb-item active">Filter Products
                        </li>
                    </ol>
                @endif
            </div>
        </div>
    </div>

    @if(count($products) > 0)
        <section id="content-types">
            {{-- <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="category">Category</label>
                        <select id="product_category" name="product_category" class="form-control">
                            <option value="" selected="" disabled="">Choose Category</option>
                            @foreach($categories as $c)
                                <option value="{{$c->id}}">{{$c->cat_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="product_subcategory">Sub-Category</label>
                        <select id="product_subcategory" name="product_subcategory" class="form-control">
                            <option value="" selected="" disabled="">Choose SubCategory</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div> --}}

            <div class="row match-height">
                @foreach($products as $p)
                    <div class="col-xl-3 col-md-6 col-sm-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-block" style="padding-top: 10px;padding-bottom: 10px;padding-right: 10px;padding-left: 10px;">
                                    <h4 class="card-title">{{$p->product_name}}</h4>
                                </div>
                        
                                <div id="carousel-example-generic{{$p->id}}" class="carousel slide" data-ride="carousel">
                                    @if(count($images->where('product_id','=',$p->id)) > 1)
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic{{$p->id}}" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic{{$p->id}}" data-slide-to="1"></li>
                                            @if(count($images->where('product_id','=',$p->id)) > 2)
                                            <li data-target="#carousel-example-generic{{$p->id}}" data-slide-to="2"></li>
                                            @endif
                                        </ol>
                                    @endif
                                    <div class="carousel-inner" role="listbox">
                                        @foreach($images->where('product_id','=',$p->id)->slice(0,3) as $i)
                                            <div class="carousel-item @if($loop->first) active @endif">
                                                <img src="{{asset('product-images'.'/'.$i->image)}}" alt="{{$p->product_name}}">
                                            </div>
                                        @endforeach
                                    </div>
                                    @if(count($images->where('product_id','=',$p->id)) > 1)
                                        <a class="left carousel-control" href="#carousel-example-generic{{$p->id}}" role="button" data-slide="prev">
                                            <span class="icon-prev" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic{{$p->id}}" role="button" data-slide="next">
                                            <span class="icon-next" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    @endif
                                </div>

                                <div class="card-block" style="padding-bottom: 0px;padding-top: 5px;padding-left: 5px;padding-right: 5px;">
                                    <div class="col-lg-4 col-xs-4">
                                        
                                        <label class="switch">
                                            
                                                <input id="active{{$p->id}}" type="checkbox"
                                                @if($p->active == true)
                                                    checked
                                                @endif
                                                >
                                                <span class="slider"></span>
                                                
                                        </label>
                                        <span><b>Active</b></span>
                                    </div>

                                    <div class="col-lg-4 col-xs-4">
                                    </div>
                                    <div class="col-lg-4 col-xs-4">
                                        <label class="switch">
                                        
                                                <input id="feature{{$p->id}}" type="checkbox"
                                                @if($p->featured == true)
                                                    checked
                                                @endif
                                                >
                                                <span class="slider"></span>
                                        </label>
                                        <span><b>Featured</b></span>
                                    </div>   
                                </div>

                                <div class="card-block" style="padding-top: 10px;">
                                    <p class="card-text">{{str_limit(strip_tags($p->product_description),100)}}</p>
                                </div>
                                
                                <div class="card-block" style="padding-bottom: 21px;padding-top: 0px;padding-left: 5px;padding-right: 5px;">
                                    <div class="col-lg-6 col-xs-6">
                                        <a href="{{ route('products.edit',$p->id) }}" class="btn btn-sm btn-primary"><i class="icon-edit2"></i> Edit</a>
                                    </div>
                                    
                                    <div class="col-lg-6 col-xs-6 pull-right">
                                        <form action="{{ route('products.destroy',$p->id) }}" method="POST" style="display:inline">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE" >
                                            <button id='deleteProduct{{ $p->id }}' type="button" class="btn btn-sm btn-danger"><i class="icon-trash-o"></i> Delete</button>
                                        </form>
                                    </div>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    @else
        <section id="content-types">
            <div class="row match-height" style="text-align:center; margin:100px 0">
                <p>No Products added yet!</p>
            </div>
        </section>
    @endif
@endsection

@section('js')
    <script>
        $('#product_category').on('change',function(e){
            var cat_id=e.target.value;
            $.get('/admin/product-subcat/' + cat_id,function(data){
                $('#product_subcategory').empty();
                $('#product_subcategory').append('<option value="none" selected="" disabled="">Choose Category</option>');
                $.each(data, function(index, subcatobj){
                    $('#product_subcategory').append('<option value="'+subcatobj.id+'">'+subcatobj.subcat_name+'</option>')
                });
            });
        });

        $('#product_subcategory').on('change',function(e){
            var subcat_id=e.target.value;
            window.location.replace("{{URL::to('admin/filter/products')}}"+'/'+subcat_id);
        });

    </script>
    <script>
        $(document).on('click',"[id*='active']",function(event){
            var id = $(this).attr("id").slice(6);
            $.ajax({
                type: "POST",
                url: "{{URL::to('admin/product/activate')}}",
                data: { 
              data: { 
                data: { 
                    id: id,
                    _token:"{{csrf_token()}}"
                },
                success: function(result) {
                
                },
                error: function(result) {
                    alert('Something went wrong.');
                }
            });

        });

        $(document).on('click',"[id*='feature']",function(event){
            var id = $(this).attr("id").slice(7);
            $.ajax({
                type: "POST",
                url: "{{URL::to('admin/product/feature')}}",
                data: { 
              data: { 
                data: { 
                    id: id,
                    _token:"{{csrf_token()}}"
                },
                success: function(result) {
                
                },
                error: function(result) {
                alert('Something went wrong.');
                }
            });
        });
    </script>
@endsection