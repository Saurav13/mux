@extends('layouts.admin')
@section('body')

<style>
    .switch input { 
        display:none;
    }
    .switch {
        display:inline-block;
        width:30px;
        height:15px;
        margin:8px;
        transform:translateY(50%);
        position:relative;
    }
    
    .slider {
        position:absolute;
        top:0;
        bottom:0;
        left:0;
        right:0;
        border-radius:15px;
        box-shadow:0 0 0 2px #777, 0 0 4px #777;
        cursor:pointer;
        border:4px solid transparent;
        overflow:hidden;
         transition:.4s;
    }
    .slider:before {
        position:absolute;
        content:"";
        width:100%;
        height:100%;
        background:#777;
        border-radius:15px;
        transform:translateX(-15px);
        transition:.4s;
    }
    
    input:checked + .slider:before {
        transform:translateX(15px);
        background:limeGreen;
    }
    input:checked + .slider {
        box-shadow:0 0 0 2px limeGreen,0 0 2px limeGreen;
    }
    
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

<div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
          <h2 class="content-header-title">Add Product</h2>
    </div>
    <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
        <div class="breadcrumb-wrapper col-xs-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ URL::to('admin/dashboard') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item"><a href="{{ URL::to('admin/products') }}">Manage Product</a>
                </li>
                <li class="breadcrumb-item active">Add Products
                </li>
            </ol>
        </div>
    </div>
</div>

<section id="basic-form-layouts">
	<div ng-app="SdpProduct" ng-controller="FixedDetailController" class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title" id="basic-layout-form"><i class="icon-android-cart"></i> Product Information</h4>
					<a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
				</div>
				<div class="card-body collapse in">
					<div class="card-block">
                    <form class="form" id="product-form" action="{{route('products.store')}}" enctype="multipart/form-data" method="POST">
                            {{csrf_field()}}
							<div class="form-body">
								<h4 class="form-section"><i class="icon-information"></i> Product Info</h4>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label for="product_name">Product Name</label>
                                            <input type="text" id="product_name" class="form-control" placeholder="Product Name" value="{{old('product_name')}}" name="product_name" required>
                                            @if ($errors->has('product_name'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('product_name') }}</strong>
                                                </div>
                                            @endif
                                        </div>
									</div>
                                </div>
        
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Regular Price</label>
                                            <input type="number" class="form-control square" name="regular_price" placeholder="Enter the price of the product" value="{{old('regular_price')}}" min="1" required />
                                            @if ($errors->has('regular_price'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('regular_price') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Sale/Discount Price (Optional)</label>
                                            <input type="number" class="form-control square" name="sale_price" placeholder="Enter the price of the product" value="{{old('sale_price')}}" min="1" />
                                            @if ($errors->has('sale_price'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('sale_price') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Quantity</label>
                                            <input type="number" class="form-control square" name="quantity" placeholder="Enter the quantity of the product" value="{{old('quantity')}}" min="1" required />
                                            @if ($errors->has('quantity'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('quantity') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Model</label>
                                            <input type="text" class="form-control square" name="model" placeholder="Enter the model of the product" value="{{old('model')}}" required />
                                            @if ($errors->has('model'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('model') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="product_description">Product Description</label>
                                    <textarea id="product_description" rows="5" class="form-control square" name="product_description" placeholder="Enter the description of the product">{{old('product_description')}}</textarea>
                                    @if ($errors->has('product_description'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('product_description') }}</strong>
                                        </div>
                                    @endif
                                </div>
								

								<h4 class="form-section"><i class="icon-cog"></i> Product Category</h4>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="product_category">Product Category</label>
											<select id="product_category" name="product_category" class="form-control" required>
                                                <option value="" selected="" disabled="">Choose Category</option>
                                                @foreach($categories as $c)
                                                    <option value="{{$c->id}}">{{$c->cat_name}}</option>
												@endforeach
                                            </select>
                                            @if ($errors->has('product_category'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('product_category') }}</strong>
                                                </div>
                                            @endif
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label for="product_subcategory">Product Sub-Category</label>
											<select id="product_subcategory" name="product_subcategory" class="form-control" required>
                                                <option value="" selected="" disabled="">Choose SubCategory</option>
												
                                            </select>
                                            @if ($errors->has('product_subcategory'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('product_subcategory') }}</strong>
                                                </div>
                                            @endif
										</div>
                                    </div>
                                    
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="product_category">Product Brand (optional)</label>
                                            <select id="product_category" name="brand" class="form-control">
                                                <option value="" {{ !old('brand') ? 'selected' : '' }} disabled="">Choose a Brand</option>
                                                @foreach($brands as $brand)
                                                    <option value="{{ $brand->id }}" {{ old('brand') == $brand->id ? 'selected' : '' }}>{{ $brand->name }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('brand'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('brand') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                
                                <h4 class="form-section"><i class="icon-image4"></i> Product Images</h4>
								<div class="form-group">
                                    <label for="product_images">Select Images</label>
                                    <input type="file" class="form-control" id="product_images" name="product_images[]" multiple required/>
                                    @if ($errors->has('product_images'))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('product_images') }}</strong>
                                        </div>
                                    @endif
                                </div>
                                	
                                <h4 class="form-section"><i class="icon-clipboard4"></i> Product Details</h4>

                                <label for="product_size_color">Product Size and Color (optional)</label>
                                <div  id="product_size_color"  class="row">
                                        <div class="col-md-6">
                                                <a href="#colors" class="btn btn-md btn-primary" ng-click="addColorfield()"> <i class="icon-plus-circle"></i> Add available colors</a>
                                            <div ng-repeat="item in colors" class="form-group" style="margin-bottom: 0px;margin-top: 5px;">
                                                    <label class="display-inline-block custom-control custom-radio ml-1">Choose Color</label>
                                                    <input ng-required="required" type="color" ng-model="item.value" style="height:1.45rem; width:8rem; " value=""/>
                                                    <a href="#delcolor" ng-click="delColor($index)" class="btn btn-sm btn-danger"><i class="icon-cross2"></i></a>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <a href="#colors" class="btn btn-md btn-primary" ng-click="addSize()"> <i class="icon-plus-circle"></i> Add available Sizes</a>
                                            <div ng-repeat="item in sizes" class="form-group" style="margin-bottom: 0px;margin-top: 5px;">
                                                    <label class="display-inline-block custom-control custom-radio ml-1"> Size</label>
                                                    <input ng-required="required" type="text" ng-model="item.value" style="height:1.45rem; width:8rem; " value=""/>
                                                    <a href="#delSize" ng-click="delSize($index)" class="btn btn-sm btn-danger"><i class="icon-cross2"></i></a>
                                                </div>
                                            </div>
                                            
                                    <input type="hidden" name="sizes" value="@{{sizes}}"/>
                                    
                                    <input type="hidden" name="colors" value="@{{colors}}"/>
                                </div>
                                <br>
                                <label for="product_size_color">Other Attributes of product (if any)</label>
                                <div class="form-group">
                                    <a href="#colors" class="btn btn-md btn-primary" ng-click="addAttributeField()"> <i class="icon-plus-circle"></i> Add attribute</a>
                                    <div class="row" ng-repeat="item in attributes" style="padding:5px;">
                                        <div class="col-md-8 col-sm-5 col-xs-5">
                                            <input ng-required="required" ng-model="item.name" type="text" class="form-control" placeholder="Attribute Name">
                                        </div>
                                        <div class="col-md-2 col-sm-3 col-xs-3">
                                            <a href="#delattribute" ng-click="delAttr($index)" class="btn btn-md btn-danger"><i class="icon-cross2"></i></a>
                                        </div> 
                                        <div class="col-md-2 col-sm-3 col-xs-3">
                                                <a href="#addvalue" ng-click="addvalue($index)" class="btn btn-md btn-primary"><i class="icon-check2">Add Value</i></a>
                                        </div> 
                                        <div ng-repeat="values in item.value track by $index" class="row">
                                            <div class="col-md-3">
                                            </div>
                                                <div style="padding:5px;" class="col-md-6 col-sm-5 col-xs-5">
                                                        <input ng-required="required" ng-model="item.value[$index]" type="text" class="form-control" placeholder="Attribute Value">
                                                </div>
                                            <div style="padding:5px;" class="col-md-2">
                                                    <a href="#delattribute" ng-click="delValue($parent.$index,$index)" class="btn btn-md btn-danger"><i class="icon-cross2"></i></a>
                                            </div>    
                                        </div>
                                    </div>
                                    <input type="hidden" name="attributes_product" value="@{{attributes}}">
                                </div>
                                <h4 class="form-section"><i class="icon-ios-checkmark-outline"></i> Feature and Activate Product</h4>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>Feature this product</label>
                                        <div class="input-group">
                                            <label class="display-inline-block custom-control custom-radio ml-1">
                                                <input type="radio" value="1" name="feature" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description ml-0">Yes</span>
                                            </label>
                                            <label class="display-inline-block custom-control custom-radio">
                                                <input type="radio" value="0" name="feature" checked class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description ml-0">No</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Show this product in website</label>
                                            <div class="input-group">
                                                <label class="display-inline-block custom-control custom-radio ml-1">
                                                    <input type="radio" value="1" name="active" class="custom-control-input" checked>
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description ml-0">Yes</span>
                                                </label>
                                                <label class="display-inline-block custom-control custom-radio">
                                                    <input type="radio" value="0" name="active"  class="custom-control-input">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description ml-0">No</span>
                                                </label>
                                            </div>
                                    </div>
                                </div>

                                <h4 class="form-section"><i class="icon-tags"></i> Product Tags</h4>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Tags</label>
                                        <select class="form-control" name="tags[]" id="selectTags" multiple="multiple">
                                            @foreach ($tags as $tag)
                                                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('tags'))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('tags') }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
							</div>

							<div class="form-actions pull-right">
                                
                                <button type="submit" id="submit-product-form" class="btn btn-primary">
                                    <i class="icon-check2"></i> Save
								</button>
                                <a class="btn btn-warning mr-1" href="{{url()->previous()}}">
                                        <i class="icon-cross2"></i>
                                    Cancel
                                </a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@section('js')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

    <script>
        var editor_config = {
            path_absolute : "/",
            selector: "textarea#product_description",
            height : 200,
            content_style: "body {font-family: Verdana,Arial,Helvetica,sans-serif;font-size: 20px;}",
            branding: false,
            menubar:true,
            toolbar: false,
            plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            height: "300",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no",
                forced_root_block : "", 
                force_br_newlines : true,
                force_p_newlines : false,
                image_class_list: [
                    {title: 'Responsive', value: 'img-responsive'}
                ],
            });
            }
        };

        tinymce.init(editor_config);
    </script>

    <script>
        $('#product_category').on('change',function(e){
            var cat_id=e.target.value;
            $.get('/admin/product-subcat/' + cat_id,function(data){
                $('#product_subcategory').empty();
                $.each(data, function(index, subcatobj){
                    $('#product_subcategory').append('<option value="'+subcatobj.id+'">'+subcatobj.subcat_name+'</option>')
                });
            });
        });
    </script>
    <script src="{{asset('admin-assets/js/addColor.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script>
        $("#selectTags").select2({
            tags: true,
            placeholder: "Select tags",
        });
    </script>
@endsection