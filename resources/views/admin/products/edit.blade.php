@extends('layouts.admin')
@section('body')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
              <h2 class="content-header-title"><i class="icon-edit2"></i> Edit Product and Product Images</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('admin/dashboard')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{URL::to('admin/products')}}">Manage Product</a>
                    </li>
                    <li class="breadcrumb-item active">Edit Product Info
                    </li>
                </ol>
            </div>
        </div>
    </div>
    <!-- Justified With Top Border start -->
    <section id="justified-top-border">
        <div class="row match-height">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <ul class="nav nav-tabs nav-justified">
                                <li class="nav-item">
                                    <a class="nav-link active" id="active-tab" data-toggle="tab" href="#active" aria-controls="active" aria-expanded="true"><h4 class="card-title" id="basic-layout-form"><i class="icon-android-cart"></i>Edit Product Information</h4>
                                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="link-tab" data-toggle="tab" href="#link" aria-controls="link" aria-expanded="false"><h4 class="card-title" id="basic-layout-form"><i class="icon-image4"></i> Product Images</h4>
                                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a></a>
                                </li>
                            </ul>
                            <div ng-app="SdpProduct" ng-controller="FixedDetailController" ng-init="Retriever({{$product}})" class="tab-content px-1 pt-1">
                                <div role="tabpanel" class="tab-pane fade active in" id="active" aria-labelledby="active-tab" aria-expanded="true">
                                    <section id="basic-form-layouts">
                                        <div class="row match-height">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-body collapse in">
                                                        <div class="card-block" style="padding-top: 0px;padding-left: 0px;padding-right: 0px;padding-bottom: 0px;">
                                                        <form class="form" action="{{route('products.update',$product->id)}}" enctype="multipart/form-data" method="POST">
                                                            <input type="hidden" name="_method" value="PATCH" />
                                                                {{csrf_field()}}
                                                                <div class="form-body" style="padding:20px;">
                                                                    <h4 class="form-section"><i class="icon-information"></i> Product Info</h4>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label for="product_name">Product Name</label>
                                                                                <input type="text" id="product_name" value="{{$product->product_name}}" class="form-control" placeholder="Product Name" name="product_name" required>
                                                                                @if ($errors->has('product_name'))
                                                                                    <div class="alert alert-danger no-border mb-2">
                                                                                        <strong>{{ $errors->first('product_name') }}</strong>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div> 
                                                                        
                                                                    </div>
                                                                    <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label>Regular Price</label>
                                                                                    <input type="number" class="form-control square" name="regular_price" placeholder="Enter the price of the product" value="{{ $product->regular_price }}" min="1" required />
                                                                                    @if ($errors->has('regular_price'))
                                                                                        <div class="alert alert-danger no-border mb-2">
                                                                                            <strong>{{ $errors->first('regular_price') }}</strong>
                                                                                        </div>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label>Sale/Discount Price (Optional)</label>
                                                                                    <input type="number" class="form-control square" name="sale_price" placeholder="Enter the price of the product" value="{{ $product->sale_price }}" min="1" />
                                                                                    @if ($errors->has('sale_price'))
                                                                                        <div class="alert alert-danger no-border mb-2">
                                                                                            <strong>{{ $errors->first('sale_price') }}</strong>
                                                                                        </div>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                        
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label>Quantity</label>
                                                                                    <input type="number" class="form-control square" name="quantity" placeholder="Enter the quantity of the product" value="{{ $product->quantity }}" min="1" required />
                                                                                    @if ($errors->has('quantity'))
                                                                                        <div class="alert alert-danger no-border mb-2">
                                                                                            <strong>{{ $errors->first('quantity') }}</strong>
                                                                                        </div>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label>Model</label>
                                                                                    <input type="text" class="form-control square" name="model" placeholder="Enter the model of the product" value="{{ $product->model }}" required />
                                                                                    @if ($errors->has('model'))
                                                                                        <div class="alert alert-danger no-border mb-2">
                                                                                            <strong>{{ $errors->first('model') }}</strong>
                                                                                        </div>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <input type="hidden" value="{{$product->id}}" name="product_id"/>
                                                                        <div class="form-group">
                                                                            <label for="product_description">Product Description</label>
                                                                            <textarea id="product_description" rows="6" class="form-control" name="product_description" placeholder="Product Description">{{$product->product_description}}</textarea>
                                                                        </div>
                                                                    
                                    
                                                                    <h4 class="form-section"><i class="icon-cog"></i> Product Category</h4>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="product_category">Product Category</label>
                                                                                <select name="product_category" id="product_category" class="form-control" required>
                                                                                    
                                                                                    @foreach($categories as $c)
                                                                                        <option @if($product->cat_id == $c->id) selected @endif value="{{$c->id}}">{{$c->cat_name}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                                @if ($errors->has('product_category'))
                                                                                    <div class="alert alert-danger no-border mb-2">
                                                                                        <strong>{{ $errors->first('product_category') }}</strong>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                    
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="product_subcategory">Product Sub-Category</label>
                                                                                <select id="product_subcategory" name="product_subcategory" class="form-control" required>
                                                                                    @foreach($subcategories->where('cat_id','=',$product->cat_id) as $c)
                                                                                        <option @if($product->subcat_id == $c->id) selected @endif value="{{$c->id}}">{{$c->subcat_name}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                                @if ($errors->has('product_subcategory'))
                                                                                    <div class="alert alert-danger no-border mb-2">
                                                                                        <strong>{{ $errors->first('product_subcategory') }}</strong>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="product_category">Product Brand (optional)</label>
                                                                                <select id="product_category" name="brand" class="form-control">
                                                                                    <option value="" {{ !$product->brand ? 'selected' : '' }} disabled="">Choose a Brand</option>
                                                                                    @foreach($brands as $brand)
                                                                                        <option value="{{ $brand->id }}" {{  $product->brand_id == $brand->id ? 'selected' : '' }}>{{ $brand->name }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                                @if ($errors->has('brand'))
                                                                                    <div class="alert alert-danger no-border mb-2">
                                                                                        <strong>{{ $errors->first('brand') }}</strong>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <h4 class="form-section"><i class="icon-image4"></i> Product Images</h4>
                                                                    <div class="form-group">
                                                                        <label for="product_images">Add more Images</label>
                                                                        <input type="file" class="form-control" id="product_images" name="product_images[]" multiple/>
                                                                        @if ($errors->has('product_images'))
                                                                            <div class="alert alert-danger no-border mb-2">
                                                                                <strong>{{ $errors->first('product_images') }}</strong>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                        
                                                                    <h4 class="form-section"><i class="icon-clipboard4"></i> Product Details</h4>

                                                                    <label for="product_size_color">Product Size and Color (optional)</label>
                                                                    <div  id="product_size_color"  class="row">
                                                                            <div class="col-md-6">
                                                                                    <a href="#colors" class="btn btn-md btn-primary" ng-click="addColorfield()"> <i class="icon-plus-circle""></i> Add available colors</a>
                                                                                <div ng-repeat="item in colors" class="form-group" style="margin-bottom: 0px;margin-top: 5px;">
                                                                                        <label class="display-inline-block custom-control custom-radio ml-1">Choose Color</label>
                                                                                        <input type="color" ng-model="item.value" style="height:1.45rem; width:8rem; " value=""/>
                                                                                        <a href="#delcolor" ng-click="delColor($index)" class="btn btn-sm btn-danger"><i class="icon-cross2"></i></a>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="col-md-6">
                                                                                <a href="#colors" class="btn btn-md btn-primary" ng-click="addSize()"> <i class="icon-plus-circle""></i> Add available Sizes</a>
                                                                                <div ng-repeat="item in sizes" class="form-group" style="margin-bottom: 0px;margin-top: 5px;">
                                                                                        <label class="display-inline-block custom-control custom-radio ml-1"> Size</label>
                                                                                        <input type="text" ng-model="item.value" style="height:1.45rem; width:8rem; " value=""/>
                                                                                        <a href="#delSize" ng-click="delSize($index)" class="btn btn-sm btn-danger"><i class="icon-cross2"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                        <input type="hidden" name="sizes" value="@{{sizes}}"/>
                                                                        <input type="hidden" name="colors" value="@{{colors}}"/>
                                                                    </div>
                                                                    <br>
                                                                    <label for="product_size_color">Other Attributes of product (if any)</label>
                                                                    <div class="form-group">
                                                                        <a href="#colors" class="btn btn-md btn-primary" ng-click="addAttributeField()"> <i class="icon-plus-circle""></i> Add attribute</a>
                                                                        <div class="row" ng-repeat="item in attributes" style="padding:5px;">
                                                                                <div class="col-md-8 col-sm-5 col-xs-5">
                                                                                    <input ng-model="item.name" type="text" class="form-control" placeholder="Attribute Name">
                                                                                </div>
                                                                                <div class="col-md-2 col-sm-3 col-xs-3">
                                                                                    <a href="#delattribute" ng-click="delAttr($index)" class="btn btn-md btn-danger"><i class="icon-cross2"></i></a>
                                                                                </div> 
                                                                                <div class="col-md-2 col-sm-3 col-xs-3">
                                                                                        <a href="#addvalue" ng-click="addvalue($index)" class="btn btn-md btn-primary"><i class="icon-check2">Add Value</i></a>
                                                                                </div> 
                                                                                <div ng-repeat="values in item.value track by $index" class="row">
                                                                                        <div style="padding:5px;" class="col-md-6 col-sm-5 col-xs-5">
                                                                                                <input style="margin-left:100px;" ng-model="item.value[$index]" type="text" class="form-control" placeholder="Attribute Value">
                                                                                        </div>
                                                                                    <div style="padding:5px; margin-left:100px; margin-top:5px;" class="col-md-2">
                                                                                            <a href="#delattribute" ng-click="delValue($parent.$index,$index)" class="btn btn-sm btn-danger"><i class="icon-cross2"></i></a>
                                                                                    </div>    
                                                                                </div>
                                                                            </div>
                                                                            <input type="hidden" name="attributes_product" value="@{{attributes}}">
                                                                        </div>
                                                                    <h4 class="form-section"><i class="icon-ios-checkmark-outline"></i> Feature and Activate Product</h4>
                                                                    <div class="form-group">
                                                                        <div class="col-md-6">
                                                                            <label>Feature this product</label>
                                                                            <div class="input-group">
                                                                                <label class="display-inline-block custom-control custom-radio ml-1">
                                                                                    <input type="radio" value="1" name="feature"
                                                                                    @if($product->featured == true)
                                                                                    checked
                                                                                    @endif
                                                                                    class="custom-control-input">
                                                                                    <span class="custom-control-indicator"></span>
                                                                                    <span class="custom-control-description ml-0">Yes</span>
                                                                                </label>
                                                                                <label class="display-inline-block custom-control custom-radio">
                                                                                    <input type="radio" value="0" name="feature" 
                                                                                    @if($product->featured == false) checked @endif 
                                                                                    class="custom-control-input">
                                                                                    <span class="custom-control-indicator"></span>
                                                                                    <span class="custom-control-description ml-0">No</span>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Show this product in website</label>
                                                                                <div class="input-group">
                                                                                    <label class="display-inline-block custom-control custom-radio ml-1">
                                                                                        <input type="radio" value="1" name="active" class="custom-control-input" @if($product->active == true) checked @endif>
                                                                                        <span class="custom-control-indicator"></span>
                                                                                        <span class="custom-control-description ml-0">Yes</span>
                                                                                    </label>
                                                                                    <label class="display-inline-block custom-control custom-radio">
                                                                                        <input type="radio" value="0" name="active" @if($product->active == false) checked @endif class="custom-control-input">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                        <span class="custom-control-description ml-0">No</span>
                                                                                    </label>
                                                                                </div>
                                                                        </div>
                                                                    </div>

                                                                    <h4 class="form-section"><i class="icon-tags"></i> Product Tags</h4>
                                                                    <div class="form-group">
                                                                        <div class="col-md-12">
                                                                            <label>Tags</label>
                                                                            <select class="form-control" name="tags[]" id="selectTags" multiple="multiple">
                                                                                @foreach ($tags as $tag)
                                                                                    <option value="{{ $tag->id }}" {{ in_array($tag->id,$product->tags()->pluck('id')->toArray()) ? 'selected' : '' }}>{{ $tag->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            @if ($errors->has('tags'))
                                                                                <div class="alert alert-danger no-border mb-2">
                                                                                    <strong>{{ $errors->first('tags') }}</strong>
                                                                                </div>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                    
                                                                <div class="form-actions pull-right" style="padding:20px;">
                                                                    <button type="button" class="btn btn-warning mr-1">
                                                                        <i class="icon-cross2"></i> Cancel
                                                                    </button>
                                                                    <button type="submit" class="btn btn-primary">
                                                                        <i class="icon-check2"></i> Save
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                            
                                </div>
                                <div class="tab-pane fade" id="link" role="tabpanel" aria-labelledby="link-tab" aria-expanded="false">
                                    <section id="header-footer">
                                        <div class="row match-height">
                                            @foreach($images as $img)
                                            <div class="col-xl-4 col-md-6">
                                                <div class="card">
                                                    <div class="card-body">
                                                    <img class="img-fluid" src="{{asset('product-images'.'/'.$img->image)}}" alt="{{$product->product_name}}">
                                                    @if(count($images) > 1)
                                                        <div class="card-block">
                                                        <a href="#" id="delete_image_product{{$img->id}}" class="btn btn-md btn-danger"><i class="icon-cross2"></i> Remove Image</a>
                                                        </div>
                                                        @endif   
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

    <script>
        var editor_config = {
            path_absolute : "/",
            selector: "textarea#product_description",
            height : 200,
            content_style: "body {font-family: Verdana,Arial,Helvetica,sans-serif;font-size: 20px;}",
            branding: false,
            menubar:true,
            toolbar: false,
            plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            height: "300",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no",
                forced_root_block : "", 
                force_br_newlines : true,
                force_p_newlines : false,
                image_class_list: [
                    {title: 'Responsive', value: 'img-responsive'}
                ],
            });
            }
        };

        tinymce.init(editor_config);
    </script>


    <script>
        $('#product_category').on('change',function(e){
            var cat_id=e.target.value;
            $.get('/admin/product-subcat/' + cat_id,function(data){
                $('#product_subcategory').empty();
                $.each(data, function(index, subcatobj){
                    $('#product_subcategory').append('<option value="'+subcatobj.id+'">'+subcatobj.subcat_name+'</option>')
                });
            });
        });
        @if(count($images) > 1)
            $(document).on('click',"[id*='delete_image_product']",function(event){
                var id = $(this).attr("id").slice(20);
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this file!",
                    type: "warning",
            
                    showCancelButton: true,
            
                }).then(function(){
                    $.post("{{route('productimage.delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
                        swal({
                        title:"Product Image Deleted Successfully",
                        type:"success"

                        }).then(function(){
                        location.reload();
                        })
                    })
                });
            });
        @endif
    </script>
    <script>
        angular.module('SdpProduct', [])
        .controller('FixedDetailController', ['$scope', function($scope){
            
            $scope.colors = [];
            $scope.sizes =[];
            $scope.attributes=[];
            $scope.addColorfield=function(){
            $scope.colors.push({})
            }
            $scope.delColor = function(i){
                $scope.colors.splice(i,1);
            }
            $scope.addSize = function(){
                $scope.sizes.push({});
            }
            $scope.delSize = function(i){
                $scope.sizes.splice(i,1);
            }
            $scope.addAttributeField=function(){
                $scope.attributes.push({name:"",value:[]});
            }
            $scope.delAttr = function(i){
                $scope.attributes.splice(i,1);
            }
            $scope.addvalue = function(i){
            $scope.attributes[i].value.push("");
            }
            $scope.delValue = function(a,i){
                $scope.attributes[a].value.splice(i,1);
            }
            $scope.Retriever = function(products)
            {   
                $scope.colors = JSON.parse(products.colors);
                $scope.sizes = JSON.parse(products.sizes);
                $scope.attributes = JSON.parse(products.details);
                console.log(products.details);
            }
        }])

    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script>
        $("#selectTags").select2({
            tags: true,
            placeholder: "Select tags",
        });
    </script>
@endsection