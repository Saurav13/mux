<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Title -->
  <title>Reset Password</title>

  <!-- Required Meta Tags Always Come First -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <!-- Favicon -->
  <link rel="shortcut icon" href="/frontend-assets/main-assets/favicon.ico">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- CSS Global Compulsory -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/vendor/animate.css">

  <!-- CSS Unify -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-core.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-components.css">
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/unify-globals.css">

  <!-- CSS Customization -->
  <link rel="stylesheet" href="/frontend-assets/main-assets/assets/css/custom.css">
  <style>
    .help-block{
        color: red;
    }
  </style>
</head>

<body>
  <main>
    <!-- Signup -->
    <section class="g-min-height-100vh g-flex-centered g-bg-img-hero g-bg-pos-top-center" style="background-image: url(/signin.jpg);">
      <div class="container g-py-50 g-pos-rel g-z-index-1">
        <div class="row justify-content-center u-box-shadow-v24">
          <div class="col-sm-10 col-md-9 col-lg-7">
            <div class="g-bg-white rounded g-py-40 g-px-30">
              <header class="text-center mb-4">
                <h2 class="h2 g-color-black g-font-weight-600">Reset Password</h2>
              </header>

              <!-- Form -->
              <form class="g-py-15" method="POST" action="{{ route('password.update') }}">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 mb-4">
                        <input id="email" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required type="email" placeholder="Email Address">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 mb-4">
                        <input id="password" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required type="password" placeholder="Password">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="col-xs-12 col-sm-6 mb-4">
                        <input id="password-confirm" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15" type="password" placeholder="Confirm Password" name="password_confirmation" required>
                    </div>
                </div>

                <div class="row justify-content-between">
                  <div class="col align-self-center text-center">
                    <button class="btn btn-md u-btn-primary rounded g-py-13 g-px-25" type="submit">{{ __('Reset Password') }}</button>
                  </div>
                </div>
              </form>
              <!-- End Form -->

            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Signup -->
  </main>

  <div class="u-outer-spaces-helper"></div>


  <!-- JS Global Compulsory -->
  <script src="/frontend-assets/main-assets/assets/vendor/jquery/jquery.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/popper.min.js"></script>
  <script src="/frontend-assets/main-assets/assets/vendor/bootstrap/bootstrap.min.js"></script>


  <!-- JS Unify -->
  <script src="/frontend-assets/main-assets/assets/js/hs.core.js"></script>

  <!-- JS Customization -->
  <script src="/frontend-assets/main-assets/assets/js/custom.js"></script>







</body>

</html>
