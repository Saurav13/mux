<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
    protected $table = "products";

    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'product_name'
            ]
        ];
    }

    public function images(){
        return $this->hasMany('App\ProductImage');
    }

    public function subcategory(){
        return $this->belongsTo('App\Subcategory','subcat_id','id');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function getPriceAttribute(){
        return $this->sale_price ? : $this->regular_price;
    }

    public function getFirstImageAttribute(){
        return $this->images->count() == 0 ? 'noimage.jpg' : 'product-images'.'/'.$this->images->first()->image;
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }
}
