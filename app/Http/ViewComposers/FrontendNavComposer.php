<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Product;
use Auth;

class FrontendNavComposer
{
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $fproducts = Product::where('active',true)->where('featured',true)->orderBy('id','desc')->limit(3)->get();
        $view->with('fproducts',$fproducts);
    }
}