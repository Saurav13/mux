<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;

class OrderController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }
    
    public function index()
    {
        $orders=Order::where('seen','0')->orderBy('id','desc')->paginate(10);
        return view('admin.orders.index')->with('orders',$orders);
    }

    public function getUnseenOrderCount(Request $request){
        
        if ($request->ajax()){
            $ordercount = Order::where('seen','0')->count();
            return response()->json(['ordercount' => $ordercount]);
        }
        abort(404);  
    }

    public function getUnseenOrder(Request $request){
        if ($request->ajax()){
            $orders = Order::where('seen','0')->latest()->limit(5)->get();
            $ordercount = Order::where('seen','0')->count();
            $view = view('admin.partials.onotis')->with('orders',$orders)->with('ordercount',$ordercount)->render(); 
            return response()->json(['html' => $view, 'ordercount' => $ordercount]);
        }
        abort(404);
    }

    public function MarkSeen(Request $request)
    {
        if($request->ajax()){
            $order=Order::where('id','=',$request->id)->first();
            $order->seen=1;
            $order->save();
            return "Marked as seen";
        }
        abort(404);
    }

    public function allOrders()
    {
       
        $orders=Order::orderBy('created_at','desc')->paginate(10);
        return view('admin.orders.allorders')->with('orders',$orders); 
    }

}
