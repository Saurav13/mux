<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;

class AdminController extends Controller
{ 
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders=Order::where('seen','=',false)->orderBy('id','desc')->paginate(10);
        return view('admin.dashboard')->with('orders',$orders);   
    }

    public function test(Request $request){
        $request->session()->flash('success', "You clicked this");
        return redirect()->route('admin_dashboard');
    }
    public function DeleteOrder(Request $request)
    {
        $order=Order::findorFail($request->id);
        $order->delete();
    }
    public function ShowOrder($id)
    {
        $order=Order::findorFail($id);

        if(!$order->seen){
            $order->seen=1;
            $order->save();
        }
        return view('admin.orders.view')->with('order',$order);
    }
}
