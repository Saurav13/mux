<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Subcategory;
use Image;
use Session;

class CategoryController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }
    public function index()
    {
        $categories=Category::orderBy('id','desc')->get();
        $subcategories=Subcategory::orderBy('id','desc')->get();
        return view('admin.categories.index')->with('categories',$categories)->with('subcategories',$subcategories);
    }

    public function StoreCat(Request $request)
    {
        $this->validate($request, array(
            'name'=>'required|max:255',
        ));
        $category=new Category;
        $category->cat_name=$request->name;
        $category->save();
    }

    public function DestroyCat(Request $request)
    {
        $this->validate($request, array(
            'id'=>'required',
        ));
        $category=Category::findorFail($request->id);
        $category->delete();
    }

    public function EditCat(Request $request)
    {
        $this->validate($request, array(
            'id'=>'required',
            'name'=>'required|max:255',
        ));
        $category=Category::find($request->id);
        $category->cat_name=$request->name;
        $category->save();
    }

    public function StoreSubCat(Request $request)
    {
        $this->validate($request, array(
            'subcat_name'=>'required|max:255',
            'category_id'=>'required',
        ));
        $subcategory= new Subcategory;
        $subcategory->subcat_name=$request->subcat_name;
        $subcategory->cat_id=$request->category_id;
        $subcategory->save();
        Session::flash('success', 'Sub-category was added');
        return redirect('admin/manage/categories');
        
    }

    public function EditSubCat(Request $request)
    {
        $this->validate($request,array(
            'id'=>'required',
            'subcat_name'=>'required|max:255',
            'category'=>'required',
        ));
        $subcategory= Subcategory::findorFail($request->id);
        $subcategory->subcat_name=$request->subcat_name;
        $subcategory->cat_id=$request->category;
        if($request->hasFile('featured_image')){
            if($subcategory->featured_image){
                unlink('subcategory-images/150x150/'.$subcategory->featured_image);
                unlink('subcategory-images/750x500/'.$subcategory->featured_image);
            }
            $file = $request->file('featured_image');
            $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
            $pathsmall = public_path('subcategory-images/150x150/'.$filename);
            $pathlarge = public_path('subcategory-images/750x500/'.$filename);
            Image::make($file)->resize(150, 150)->save($pathsmall);
            Image::make($file)->resize(750, 500)->save($pathlarge);
            $subcategory->featured_image = $filename;
         }
        $subcategory->save();
        Session::flash('success', 'Sub-category was Updated');
        return redirect('admin/manage/categories');
    }

    public function DestroySubCat(Request $request)
    {
        $this->validate($request,array(
            'id'=>'required',
        ));
        $subcategory=Subcategory::findorFail($request->id);
        unlink('subcategory-images/150x150/'.$subcategory->featured_image);
        unlink('subcategory-images/750x500/'.$subcategory->featured_image);
        $subcategory->delete();
    }
}
