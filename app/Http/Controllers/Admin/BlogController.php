<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rules\NoEmptyContent;
use App\Blog;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $blogs = Blog::orderBy('updated_at','desc')->paginate(10);
        
        return view('admin.blogs.index')->with('blogs',$blogs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([          
            'content' => ['required', new NoEmptyContent],
            'title' => 'required',
            'author' => 'required',
            'image' => 'required|image'
        ]);

        $blog = new Blog;
        $blog->title = $request->title;
        $blog->author = $request->author;
        $blog->content = $request->content;

        if($request->hasFile('image')){
            $photo = $request->file('image');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('blog_images/');
            $photo->move($location,$filename);
            $blog->image = $filename;
        }

        $blog->save();

        $request->session()->flash('success', 'Blog added.');        
        
        return redirect()->route('blogs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blog::findOrFail($id);

        return view('admin.blogs.show',compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::findOrFail($id);

        return view('admin.blogs.edit',compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([          
            'content' => ['required', new NoEmptyContent],
            'title' => 'required',
            'author' => 'required',
            'image' => 'nullable|image'
        ]);

        $blog = Blog::findOrFail($id);
        $blog->title = $request->title;
        $blog->author = $request->author;
        $blog->content = $request->content;

        if($request->hasFile('image')){

            if($blog->image)
                unlink(public_path('blog_images/'.$blog->image));

            $photo = $request->file('image');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('blog_images/');
            $photo->move($location,$filename);
            $blog->image = $filename;
        }

        $blog->save();

        $request->session()->flash('success', 'Blog updated.');        
        
        return redirect()->route('blogs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $blog = Blog::findOrFail($id);
        $blog->delete();

        $request->session()->flash('success', 'Blog deleted.');        
        
        return redirect()->route('blogs.index',array('page'=>$request->page));
    }
}
