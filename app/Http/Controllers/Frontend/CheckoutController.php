<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart;

class CheckoutController extends Controller
{
    public function summary()
    {
        $cart_items = Cart::content();

        if(count($cart_items) == 0)
            return redirect('cart');

        return view('frontend.checkout')->with('cart_items',$cart_items);
    }
    
}
