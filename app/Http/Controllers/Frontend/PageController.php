<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Subcategory;
use App\Product;
use App\ProductImage;
use Session;
use App\ContactUsMessage;
use App\Order;
use App\Testimonial;
use App\Subscriber;
use App\LandingImage;
use App\Blog;
use Validator;
use App\Tag;
use App\Brand;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class PageController extends Controller
{
    public function home()
    {
        $featured_products=Product::where('active','=',true)->where('featured','=',true)->get();
        $images=collect([]);
        $testimonials =Testimonial::all();
        $landing_images = LandingImage::all();
        $posts = Blog::orderBy('updated_at','desc')->limit(3)->get();

        foreach($featured_products as $p){
            $image=ProductImage::where('product_id','=',$p->id)->get();
            $images=$images->merge($image);   
        }
        
        return view('frontend.landing')->with('posts',$posts)->with('featured_products',$featured_products)->with('images',$images)->with('testimonials',$testimonials)->with('landing_images',$landing_images);
    }

    public function category($slug)
    {
        $category=Category::where('slug',$slug)->first();
        $subcategories=Subcategory::where('cat_id',$category->id)->get();
        $products=Product::where('active','=',true)->get();
        $images=ProductImage::all();
        return view('frontend.category')->with('category',$category)->with('subcategories',$subcategories)->with('products',$products)->with('images',$images);
    }

    public function products($slug)
    {   
        $product = Product::where('active','=',true)->where('slug','=',$slug)->first();
        if(!$product) abort(404);

        $similar = Product::where('active','=',true)->where('subcat_id',$product->subcat_id)->where('id','!=',$product->id)->orderBy('created_at','desc')->limit(4)->get();

        $images=ProductImage::where('product_id','=',$product->id)->get();
        return view('frontend.product')->with('product',$product)->with('images',$images)->with('similar',$similar);
    }

    public function shop(Request $request, Product $product){
        $product = $product->newQuery();

        if($request->search){
            $tags = explode(' ',$request->search);

            $product->join('product_tag', 'product_tag.product_id', 'products.id')
                ->join('tags', 'product_tag.tag_id', 'tags.id')
                ->where(function ($query) use ($tags) {
                    $queryMethod = 'where';

                    if (is_array($tags)) {
                        $queryMethod = 'whereIn';
                    }

                    $orQueryMethod = 'or' . ucfirst($queryMethod);

                    $query->{$queryMethod}('tags.slug', $tags)
                        ->{$orQueryMethod}('tags.id', $tags);
                })
                ->select('products.*')->distinct();
        }

        if($request->filter)
        {
            $product->whereHas('subcategory', function ($query) use ($request) {
                $query->where('subcategories.slug', $request->input('filter'));
            });  
        }

        if(is_array($request->price)){
            $product->where('regular_price','>=',$request->price['min'])->where('regular_price','<=',$request->price['max']);     
        }
        
        if(is_array($request->brands)){
            $product->whereHas('brand', function ($query) use ($request) {
                $query->whereIn('brands.slug', $request->input('brands'));
            });
        }
        
        $products = $product->orderBy('products.id','desc')->paginate(9);  
       
        $categories = Category::all();
        $brands = Brand::has('products')->get();
    
        return view('frontend.products',compact('products','categories','brands'));
        
    }

    // public function Search(Request $request)
    // {
    //     $query=$request->search;
    //     $subcat=Subcategory::all();
    //     $products=Product::where('active','=',true)->where('product_name', 'LIKE', "%$query%")->paginate(10);
        
    //     return view ('frontend.search')->with('products',$products)->with('query',$query)->with('subcat',$subcat);
    // }

    public function Contact(Request $request)
    {  
        $this->validate(
            $request, 
            [   
                'email'             => 'required|email',
                'name'          => 'required|max:255',
                'number' => 'required|numeric',
                'message' => 'required|max:500',
                'subject'=>'required',
                
            ],
            [   
                'email.required'    => 'Please Provide Your Email Address.',
                'email.email'      => 'Sorry, Your email is not a valid email',
                'name.required' => 'Name is required.',
                'name.max'      => 'Your name should be less than 255 charcaters',
                'number.required' => 'Number is required',
                'number.numeric' => 'Enter a valid number.',
                'message' => 'A message is required',
            ]
        );

        $contact= new ContactUsMessage;
        $contact->name= $request->name;
        $contact->email=$request->email;
        $contact->subject=$request->subject;
        $contact->message=$request->message;
        $contact->phone=$request->number;
        $contact->seen=false;
        $contact->save();
        Session::flash('success','Your Message was sent.');
        return redirect("/");


    }
    
    public function Order($slug)
    {
        $product=Product::where('slug','=',$slug)->first();
        $image=Productimage::where('product_id','=',$product->id)->first();
        return view('frontend.order')->with('product',$product)->with('image',$image);

    }

    public function GeneralOrder(Request $request)
    {
        $ordereq = (object)$request->order;

        if(isset($ordereq->notsure)){
            $validator = Validator::make($request->order, 
                [   
                    'email' => 'nullable|email',
                    'name' => 'required|max:100',
                    'phone' => 'required|numeric',
                    'address' => 'required|max:500',    
                ],
                [ 
                    'email.email'      => 'Sorry, Your email is not a valid email',
                    'name.required' => 'Your name is required.',
                    'name.max'      => 'Your name should be less than 100 charcaters',
                    'phone.required' => 'Contact Number is required',
                    'phone.numeric' => 'Enter a valid Contact number.'
                ]
            );
        }else{
            $validator = Validator::make($request->order, 
                [   
                    'email' => 'nullable|email',
                    'name' => 'required|max:100',
                    'phone' => 'required|numeric',
                    'address' => 'required|max:500',
                    'quantity' => 'required|min:1|integer',      
                ],
                [ 
                    'email.email'      => 'Sorry, Your email is not a valid email',
                    'name.required' => 'Your name is required.',
                    'name.max'      => 'Your name should be less than 100 charcaters',
                    'phone.required' => 'Contact Number is required',
                    'phone.numeric' => 'Enter a valid Contact number.',
                    'quantity.min' => 'Number of rolls cannot be less than 1.'
                ]
            );
        }

        if($validator->fails()){
            if($request->ajax())
                return response()->json(['errors'=>$validator->errors()],422);
            else {
                return redirect()->back()->withErrors($validator)->withInput();
            }
        }
        
        $product = Product::where('active',true)->findOrFail($ordereq->product_id);

        $order= new Order;
        $order->name= $ordereq->name;
        $order->email=$ordereq->email;
        $order->phone=$ordereq->phone;
        $order->address=$ordereq->address;
        $order->message=$ordereq->message;

        if(isset($ordereq->notsure)){
            $order->quantity = 0;
            $quantity = 1;
        }
        else{
            $order->quantity = $ordereq->quantity;
            $quantity = $ordereq->quantity;
        }

        
        $order->product_name=$product->product_name;
        $order->price = $product->price * $quantity;
        $order->seen=false;
        
        $order->save();

        if($request->ajax()){
            return 'success';
        }else{
            Session::flash('ordered','Your order is received. We will get back to you within 24 hours.');
            return redirect("/");
        }
        
    }
    public function NewsLetter(Request $request)
    {
        $subscriber=new Subscriber;
        $subscriber->email=$request->email;
        $subscriber->token=$request->_token;
        $subscriber->save();
        Session::flash('success','Thank you for joining our newsletter!');
        return redirect("/");

    }

    public function Search(Request $request){
        if(!$request->search){
            return redirect('/');
        }

        if($request->search){
            $tags = explode(' ',$request->search);

            $result = collect();
            $matches = [];
            $count = 0;
            
            //search by tags
            foreach(Product::all() as $p){
                $match = count(array_intersect(array_map('strtolower', $p->tags()->pluck('name')->toArray()),array_map('strtolower', $tags)));
                if($match == 0) continue;
                $matches[$match] []= $p;
                $count ++;
            }

            krsort($matches);
            
            foreach($matches as $match=>$products){
                foreach($products as $p){
                    $result =  $result->merge([$p]);
                }
            }
            dd($result);
		
		
            // $results = $this->paginate($result, $perPage = 10, $page = null, $options = ['path' => route('shop',['search'=> implode(' ',$tags)])]);
        }
        $categories = Category::all();

		return view('frontend.shop')->with('products',$results)->with('categories',$categories);
	}

	public function paginate($items, $perPage = 15, $page = null, $options = [])
	{
		$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
		$items = $items instanceof Collection ? $items : Collection::make($items);
		return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
	}

}
