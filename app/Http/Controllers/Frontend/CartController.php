<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Order;
use Illuminate\Support\Str;
use Mail;
// use App\Mail\CustomerInvoiceMail;
use App\Product;
use App\ProductImage;
use Session;
use Cart;
use Auth;

class CartController extends Controller
{

    public function addItem(Request $request)
    {
        $request->validate([
            'id' => 'required|exists:products,id',
            'quantity' => 'required|integer|min:1'
        ]);

        $product = Product::findOrFail($request->id);
        // $details=json_decode($product->details);
        $qty = $product->quantity;

        // $options=(array)json_decode($request->details);
        $qty_buy = $request->quantity ? (int)$request->quantity : 1;

        //checking if required product is out of stock
        // foreach ($details as $d) {
        //     if($d->size==$options['size'] && $d->color==$options['color'])
        //         $qty = $d->quantity;
        // }

        // if($qty == -1){
        //     return response()->json(['error' => 'Product unavailable'], 404);
        // }

        if($qty == 0 || $qty < $qty_buy)
        {
            return response()->json(['error' => 'Out of Stock'], 422);
        }
        //checking if the added product quantity exceeds the remaining quantity
        foreach (Cart::content() as $c) {
             
            //  if($c->options->size==$options['size'] && $c->options->color==$options['color'] && $c->id==$request->id )
            //     {
                    if($c->qty + $qty_buy > $qty)
                        return response()->json(['error' => 'Out of Stock'], 422);
                // }
        }
        
        // if($options['color']!=""){
        //     $itm = Product_Image::where('product_id',$product->product_id)->where('color',$options['color'])->first();
        //     $options['image'] = $itm ? $itm->image_link : '';
        // }
        // else{
        //     $itm = Product_Image::where('product_id',$product->product_id)->first();
        //     $options['image'] = $itm ? $itm->image_link : '';
        // }

        Cart::add(['id'=>$product->id,'name'=>$product->product_name,'qty'=>$qty_buy,'price'=>$product->price, 'options' => ['image' => $product->firstImage ]]);       

        return response()->json(['cart' => Cart::content(), 'count' => Cart::content()->count()], 200);

    }

    public function emptyCart()
    {
        Cart::destroy();
        return redirect('cart');      
    }

    public function updateQty(Request $request)
    {   
        $request->validate([
            'quantity' => 'required|integer|min:1'
        ]);

        $product=Cart::get($request->rowId);

        $qty = Product::find($product->id)->quantity;

        if($request->quantity < 0)
        {
            return response()->json(['error' => 'Invalid Entry'], 422);   
        }
        if($request->quantity > 10)
        {
             return response()->json(['error' => 'Exceeds Purchase Limit'], 422);   
        }

        if((int)$request->quantity-$request->quantity!=0)
        {
            return response()->json(['error' => 'Input integer'], 422);   
        }

        // foreach ($details as $d) {
        //      if($d->size==$product->options->size && $d->color==$product->options->color)
        //         $qty=$d->quantity;
        // }
        if($request->quantity <= $qty)
        {
            Cart::update($request->rowId, $request->quantity); 
            return response()->json(['item' => Cart::get($request->rowId), 'total' => Cart::subtotal()], 200);
        }
        else
        {
            return response()->json(['error' => 'Out of Stock'], 422);
        }
    }

    public function removeItem(Request $request)
    {
        Cart::remove($request->rowId);
        return response()->json(['rowId' => $request->rowId, 'total' => Cart::subtotal(), 'count' => Cart::content()->count()], 200);
    }

    public function cart()
    {
        $cart_items = Cart::content();

        if(count($cart_items) == 0)
            return view('frontend.emptycart');

        return view('frontend.cart')->with('cart_items',$cart_items);
    }
    
    public function purchase(Request $request)
    {
        $cart = Cart::content();
        if(count($cart)==0){
            Session::flash('error','Your cart is empty');
            return redirect()->route('cart');
        }

        $total = 0;
        $purchases = [];

        foreach($cart as $c){
            $product = (object) null;
            $product->productId = $c->id;
            $product->product = $c->name;
            $product->quantity = $c->qty;
            $product->price = $c->options['price'];
            $product->details = $c->options;
            $total += $product->price*$product->quantity;
            $purchases []= $product;
        }
        if(!self::CheckAvailability($purchases))
            return redirect()->route('cart');
        
        $transaction = new Transaction;
        $transaction->details = json_encode($purchases);
        $transaction->total = $total;
        $transaction->status = 'pending';
        $transaction->code = Str::random(20);
        $transaction->save();
        return redirect()->route('cart.payment',$transaction->code);
    }

    public function payment($code)
    {
        $transaction = Transaction::where('code','=',$code)->first();
        
        if(!$transaction)
            abort(404);
        if($transaction->status!= 'pending'){
            Session::flash('success', 'The transaction has already been completed');
            return redirect('/collection');
        }

        $countries = Country::orderBy('name')->get();

        return view('cart.payment')->with('transaction',$transaction)->with('countries',$countries);
    }

    public function shippingInformation(Request $request,$code){
        $this->validate($request,array(
            'fname' => 'sometimes|required|max:191',
            'lname' => 'sometimes|required|max:191',
            'email' => 'sometimes|required|email',
            'shipping_contact' => 'required',
            'shipping_address' => 'required|max:191',
            'apt_suit' => 'max:191',
            'suburb' => 'required|max:191',
            'country' => 'required|max:191|exists:countries,name',
            'state' => 'required|max:191',
            'postcode' => 'required|numeric',
            'city' => 'required'
        ));
        
        $transaction = Transaction::where('code','=',$code)->first();
        if(!$transaction) abort(404);
        
        if($transaction->status!= 'pending'){
            Session::flash('success', 'The transaction has already been completed');
            return redirect('/collection');
        }
        
        if(Auth::check()){
            $transaction->transactionable_id = Auth::user()->id;
            $transaction->transactionable_type = get_class(Auth::user());
        }
        else{
            $guest = new Guest;
            $guest->name = $request['fname'] . ' ' . $request['lname'];
            $guest->email = $request['email'];
            $guest->phone = $request['shipping_contact'];
            $guest->save();
            $transaction->transactionable_id = $guest->id;
            $transaction->transactionable_type = get_class($guest);

            if($request->has('subscribeme')){
                if (!Subscriber::where('email', '=', $request->email)->exists()) {
                    $subscriber = new Subscriber();
                    $subscriber->email = $request->email;
                    $subscriber->token = Str::random(60);
                    $subscriber->save();
                }
            }
        }
        $transaction->shipping_contact = $request['shipping_contact'];
        $transaction->shipping_address = $request['shipping_address'];
        $transaction->companyName = $request['companyName'];
        $transaction->apt_suit = $request['apt_suit'];
        $transaction->city = $request['city'];
        $transaction->country = $request['country'];
        $transaction->state = $request['state'];
        $transaction->suburb = $request['suburb'];
        $transaction->postcode = $request['postcode'];
        if(isset($request['agentId']))
        {
            if(Agent::where('agent_code',$request['agentId'])->where('agent_status','active')->count()>0){
                $transaction->agent_code=$request['agentId'];
            }
        }
         
        $transaction->save();

        return redirect()->route('billingform',$code);
    }

    // public function CheckAvailability($purchases){
    //     foreach ($purchases as $purchase) { 
            
    //         $product = Product::find($purchase->productId);
            
    //         if(!$product){
    //             Session::flash('error', $purchase->product . ' is no longer available.');
    //             return false;
    //         }
            
    //         $products = json_decode($product->details);
    //         $found = 0;
    //         foreach ($products as $product) {
    //             if($product->color == $purchase->details->color && $product->size == $purchase->details->size)
    //             {
    //                 $found = 1;
    //                 if($purchase->quantity > $product->quantity){
    //                     $size = '(' . $purchase->details->size . ')';
    //                     if($size=='()')
    //                         $size='';
    //                     $det = $purchase->product .'-'.$purchase->details->color . $size;
               
    //                     Session::flash('error', $det . ' only ' . $product->quantity . ' in stock');
    //                     return false;
    //                 }
    //             }
    //         }
    //         if($found == 0){
    //             $size = '(' . $purchase->details->size . ')';
    //             if($size=='()')
    //                 $size='';
                    
    //             $det = $purchase->product .'-'.$purchase->details->color . $size;
    //             Session::flash('error', $det . 'is no longer available.');
    //             return false;
    //         }
    //     }
    //     return true;
    // }

    public function paymentSuccess($code){
        $transaction = Transaction::where('code','=',$code)->first();

        if(!$transaction) abort(404);
        
        if($transaction->status!= 'pending'){
            Session::flash('success', 'The transaction has already been completed');
            return redirect('/collection');
        }
        
        if($transaction->total!=0){
            return view('cart.paywithpaypal')->with('transaction',$transaction);
        }
        
        $purchases = json_decode($transaction->details);

        if(!self::CheckAvailability($purchases))
            return redirect()->route('cart');
            
        //change gift cards status if exists
        if($transaction->giftCard_id!=''){
            $transaction->giftCard->status = 'used';
            $transaction->giftCard->save();
        }

        //update quantity

        foreach ($purchases as $purchase) { 

            $product = Product::find($purchase->productId);
            $products = json_decode($product->details);
            
            foreach ($products as $product) {
                if($product->color == $purchase->details->color && $product->size == $purchase->details->size)
                {
                    $product->quantity = $product->quantity - $purchase->quantity;
                    if($product->quantity<0) $product->quantity = 0;
                    $product->details = json_encode($products);
                    $product->save();
                    break;
                }
            }
        }
        $transaction->status = 'completed';
        $transaction->save();

        Mail::to($transaction->transactionable->email)->send(new CustomerInvoiceMail($transaction));
        Session::flash('success','Payment successful. Thank you for Ordering. Invoice has been sent to your email address.');
        return redirect()->route('collection');
    }
}
