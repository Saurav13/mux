<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;

class BlogController extends Controller
{
    public function index()
    {
        $posts = Blog::orderBy('updated_at','desc')->paginate(10);
        return view('frontend.blog',compact('posts'));
    }

    public function post($alias){
        $post = Blog::where('slug',$alias)->first();
        if(!$post) abort(404);

        return view('frontend.post',compact('post'));
    }
}
