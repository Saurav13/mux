<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Subcategory extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'subcat_name'
            ]
        ];
    }

    public function category(){
        return $this->belongsTo('App\Category','cat_id');
    }

    public function products()
    {
        return $this->hasMany('App\Product', 'subcat_id');
    }
}
