
function themeNotify(level, message) {
    
    if (undefined === level && undefined === message) {

        var level = 'error';
    } else {
        level = level;
        message = message;
    }

    if (undefined === level && undefined === message) {
        level = 'error';
        message = 'Something went wrong!!';
    }

    var icon = 'fa fa-bell';

    if (level === 'error') {
        level = 'danger';
        icon = 'fa fa-minus-circle';
    }

    if (level === 'success') {
        icon = 'fa fa-check-circle-o';
    }

    var alert = $('<div class="alert alert-'+level+' alert-dismissible fade show" role="alert" data-notify-position="bottom-right" style="display: inline-block; margin: 10px 0; position: relative; transition: all 0.5s ease-in-out; z-index: 1031; right: 10px; animation-iteration-count: 1;padding: 15px;min-width: 20%;">'+
                    
                    '<div class="media">'+
                    '<span class="d-flex mr-2 g-mt-5">'+
                        '<i class="'+icon+'"></i>'+
                    '</span>'+
                    '<div class="media-body">'+
                        message+
                    '</div>'+
                    '</div>'+
                '</div><br>');

    $("#alertStack").prepend(alert);

    setTimeout(() => {
        alert.remove();
    }, 3000);
}

function themeAlert(content,title,type){
    // $('#alerttype').html(type);
    $('#alerttitle').html(title);
    $('#alertcontent').html(content);

    var modal = new Custombox.modal({
        content: {
            effect: 'fadein',
            target: '#messageModal'
        }
    });

    // Open
    modal.open();
}

function closeAlert(){
    $('#alerttitle').empty();
    $('#alertcontent').empty();
    
    var modal = new Custombox.modal({
        content: {
            effect: 'fadein',
            target: '#messageModal'
        }
    });

    // Open
    modal.close();
}

function showErrors(response){
    var message = '';

    if(response.responseJSON.error)
        message = response.responseJSON.error;
    else if(response.status == 422){
        $.each(response.responseJSON.errors, function (key, val) {
            message += val + "<br>";
        });
    }
    else if(response.status == 404)
        message = 'Invalid Action';
    else if(response.status == 0)
        message = 'Please Check your Internet Connection.';
    else
        message = 'Something Went Wrong';
    
    themeNotify('error',message);
    
}